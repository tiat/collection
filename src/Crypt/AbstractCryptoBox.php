<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Jantia\Stdlib\Message\Message;
use Psr\Log\LogLevel;
use Tiat\Collection\Crypt\Ext\CryptoBoxExtensionInterface;
use Tiat\Collection\Crypt\Ext\CryptoBoxExtensionOpenssl;
use Tiat\Collection\Crypt\Ext\CryptoBoxExtensionSodium;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;

use function array_key_exists;
use function array_keys;
use function file_exists;
use function implode;
use function in_array;
use function is_dir;
use function is_readable;
use function sprintf;
use function strtolower;
use function substr;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractCryptoBox extends Message implements CryptoBoxInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string WARNING_PATH_NOT_EXISTS = 'path_not_exists';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array EXTENSION_OPTIONS = ['options' => NULL, 'base64' => NULL,
	                                              self::RANDOM_BYTES_KEY_IV => NULL,
	                                              self::RANDOM_BYTES_KEY_TAG => NULL];
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_extensionOptions = self::EXTENSION_OPTIONS;
	
	/**
	 * @var array|string[][]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_templates = [LogLevel::WARNING => [self::WARNING_PATH_NOT_EXISTS => "Path doesn't exists or it's not readable."]];
	
	/**
	 * Used encryption mode (default: asymmetric = private/public key)
	 *
	 * @var CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	private CryptoBoxParams $_cryptoMode = self::CRYPTO_DEFAULT_MODE;
	
	/**
	 * @var CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	private CryptoBoxParams $_extension = self::CRYPTO_DEFAULT_EXTENSION;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoPemFilePublic = self::DEFAULT_CRYPTO_PEM_FILE_PUBLIC;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoPemFilePrivate = self::DEFAULT_CRYPTO_PEM_FILE_PRIVATE;
	
	/**
	 * Used CryptoBox extension (default: openssl)
	 *
	 * @var null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	private ?CryptoBoxExtensionInterface $_cryptoBoxExtension;
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_certPath;
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_cryptoPassword;
	
	/**
	 * @param    null|string    $certPath
	 * @param    mixed          ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?string $certPath = NULL, ...$args) {
		//
		$this->setMessageTemplates($this->_templates);
		
		// Set the certification(s) path if defined
		if($certPath !== NULL):
			$this->setCertPath($certPath);
		endif;
		
		//
		parent::__construct(...$args);
	}
	
	/**
	 * @param    string             $filename
	 * @param    CryptoBoxParams    $fileType
	 * @param    null|string        $password
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBoxPemFile(string $filename, CryptoBoxParams $fileType, ?string $password = NULL) : static {
		//
		if($fileType === self::CRYPTO_PEM_FILE_PUBLIC):
			$this->_cryptoPemFilePublic = $filename;
		elseif($fileType === self::CRYPTO_PEM_FILE_PRIVATE):
			//
			$this->_cryptoPemFilePrivate = $filename;
			
			// Set password if not EMPTY
			if(! empty($password)):
				$this->setCryptoPassword($password);
			endif;
		else:
			//
			$msg =
				sprintf("Given filetype (%s) is not allowed. Allowed types are CryptoBox values %s.", $fileType->value,
				        implode(', ', self::CRYPTO_PEM_FILE_ACCEPT));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoPassword() : static {
		//
		unset($this->_cryptoPassword);
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetExtension() : CryptoBoxInterface {
		//
		unset($this->_extension);
		
		//
		return $this;
	}
	
	/**
	 * @return null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBoxExtension() : ?CryptoBoxExtensionInterface {
		// If extension is not defined then use the default one
		if(empty($this->_cryptoBoxExtension)):
			$this->setCryptoBoxExtension($this->getCryptoBoxExtensionInterface($this->getExtension() ??
			                                                                   $this->getExtensionDefault()));
		endif;
		
		//
		return $this->_cryptoBoxExtension ?? NULL;
	}
	
	/**
	 * @param    CryptoBoxExtensionInterface    $interface
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBoxExtension(CryptoBoxExtensionInterface $interface) : CryptoBoxInterface {
		//
		$this->_cryptoBoxExtension = $interface;
		
		//
		return $this;
	}
	
	/**
	 * @param    CryptoBoxParams    $value
	 *
	 * @return null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBoxExtensionInterface(CryptoBoxParams $value) : ?CryptoBoxExtensionInterface {
		return match ( $value ) {
			CryptoBoxParams::CRYPTO_EXTENSION_OPENSSL => new CryptoBoxExtensionOpenssl($this->getCertPath()),
			CryptoBoxParams::CRYPTO_EXTENSION_SODIUM => new CryptoBoxExtensionSodium($this->getCertPath()),
			default => throw new InvalidArgumentException(sprintf("Given interface (%s) is not valid.", $value->value))
		};
	}
	
	/**
	 * Return certificate path
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCertPath() : ?string {
		return $this->_certPath ?? NULL;
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPath(string $path) : static {
		//
		if(! empty($path)):
			if(file_exists($path)):
				if(is_dir($path)):
					if(is_readable($path) === TRUE):
						$this->_certPath = $path;
					else:
						$msg = sprintf("Given path (%s) exists but is not readable", $path);
						throw new InvalidArgumentException($msg);
					endif;
				else:
					$msg = sprintf("Given path (%s) exists but is not directory", $path);
					throw new InvalidArgumentException($msg);
				endif;
			else:
				$msg = sprintf("Given path (%s) doesn't exists", $path);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			// Set WARNING message that cert path is empty or doesn't exist
			$msg = sprintf("Cert path %s doesn't exists or it's empty", $path);
			$this->setMessage(self::WARNING_PATH_NOT_EXISTS, [$msg]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getExtension() : ?CryptoBoxParams {
		return $this->_extension;
	}
	
	/**
	 * @param    CryptoBoxParams    $value
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setExtension(CryptoBoxParams $value) : CryptoBoxInterface {
		//
		if(in_array($value, self::CRYPTO_EXTENSION_ALLOWED, TRUE) === TRUE):
			$this->_extension = $value;
		else:
			$msg = sprintf("Given value (%s) is not valid value", $value->value);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getExtensionDefault() : CryptoBoxParams {
		return self::CRYPTO_DEFAULT_EXTENSION;
	}
	
	/**
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoBoxExtension() : CryptoBoxInterface {
		//
		unset($this->_cryptoBoxExtension);
		
		//
		return $this;
	}
	
	/**
	 * @return null|CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMode() : ?CryptoBoxParams {
		return $this->_cryptoMode ?? NULL;
	}
	
	/**
	 * @param    CryptoBoxParams    $param
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoMode(CryptoBoxParams $param) : static {
		//
		if(in_array($param, self::CRYPTO_MODE_ALLOWED, TRUE) === TRUE):
			$this->_cryptoMode = $param;
		else:
			$msg = sprintf("Given value (%s) is not valid value", $param->value);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoMode() : CryptoBoxInterface {
		//
		$this->_cryptoMode = self::CRYPTO_DEFAULT_MODE;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $options
	 * @param    bool     $overwrite
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setOptions(array $options, bool $overwrite = FALSE) : static {
		if(! empty($options)):
			foreach($options as $key => $val):
				$this->setOption($key, $val, $overwrite);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $key
	 * @param    mixed     $value
	 * @param    bool      $overwrite
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setOption(string $key, mixed $value, bool $overwrite = FALSE) : static {
		if(array_key_exists(strtolower($key), self::EXTENSION_OPTIONS)):
			if(! isset($this->_extensionOptions[$key]) || $overwrite === TRUE):
				$this->_extensionOptions[$key] = $value;
			else:
				$msg = sprintf("Value for key '%s' already exists and overwrite is not allowed.", $key);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			$msg = sprintf("Given key (%s) is not a valid key. Allowed keys are: %s", $key,
			               implode(', ', array_keys(self::EXTENSION_OPTIONS)));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array|null[]|string[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getOptions() : ?array {
		return $this->_extensionOptions ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetOptions() : static {
		//
		unset($this->_extensionOptions);
		
		//
		return $this;
	}
	
	/**
	 * @param    string        $key
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getOption(string $key, mixed $default = NULL) : mixed {
		return $this->_extensionOptions[$key] ?? $default;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetOption(string $key) : CryptoBoxInterface {
		//
		unset($this->_extensionOptions[$key]);
		
		//
		return $this;
	}
	
	/**
	 * @param    CryptoBoxParams    $fileType
	 *
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getCryptoBoxPemFile(CryptoBoxParams $fileType) : array|string|null {
		// Check that there is the path defined for certificate(s)
		if(! empty($this->getCertPath())):
			if(substr($path = $this->getCertPath(), -1, 1) !== DIRECTORY_SEPARATOR):
				$path .= DIRECTORY_SEPARATOR;
			endif;
		else:
			throw new RuntimeException("There is no path defined for certificates.");
		endif;
		
		// First test that if the PEM is string then return it as is
		if($this->_isPemString($pem = $this->getCryptoBoxPemFile($fileType)) === TRUE):
			return $pem;
		elseif($fileType === self::CRYPTO_PEM_FILE_PUBLIC):
			//
			if(! empty($filename = $this->getCryptoBoxPemFile($fileType))):
				return ['filename' => $path . $filename, 'passphrase' => $this->_getCryptoPassword()];
			else:
				return NULL;
			endif;
		elseif($fileType === self::CRYPTO_PEM_FILE_PRIVATE):
			//
			if(! empty($filename = $this->getCryptoBoxPemFile($fileType))):
				return ['filename' => $path . $filename, 'passphrase' => $this->_getCryptoPassword()];
			else:
				return NULL;
			endif;
		else:
			//
			$msg =
				sprintf("Given filetype (%s) is not allowed. Allowed types are CryptoBox values %s.", $fileType->value,
				        implode(', ', self::CRYPTO_PEM_FILE_ACCEPT));
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string    $pem
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	private function _isPemString(string $pem) : bool {
		// Simple test: check if the string starts with '-----BEGIN' and ends with '-----END'.
		return str_contains($pem, '-----BEGIN') && str_contains($pem, '-----END');
	}
	
	/**
	 * @param    CryptoBoxParams    $fileType
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBoxPemFile(CryptoBoxParams $fileType) : string {
		// Set filename without path
		return match ( $fileType ) {
			CryptoBoxParams::CRYPTO_PEM_FILE_PUBLIC => $this->_cryptoPemFilePublic,
			CryptoBoxParams::CRYPTO_PEM_FILE_PRIVATE => $this->_cryptoPemFilePrivate,
			default => throw new InvalidArgumentException(sprintf("Filetype %s is not supported.", $fileType->value))
		};
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getCryptoPassword() : ?string {
		return $this->_cryptoPassword ?? NULL;
	}
	
	/**
	 * @param    string    $password
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoPassword(string $password) : static {
		//
		if(! empty($password)):
			$this->_cryptoPassword = $password;
		endif;
		
		//
		return $this;
	}
}
