<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CryptoTraitInterface {
	
	/**
	 * Get message from/to encrypt/decrypt
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMessage() : mixed;
	
	/**
	 * Set message from/to encrypt/decrypt
	 *
	 * @param    mixed    $message
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoMessage(mixed $message) : CryptoTraitInterface;
	
	/**
	 * Reset the message from/to encrypt/decrypt
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoMessage() : CryptoTraitInterface;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCipher() : string;
	
	/**
	 * Set used cipher
	 *
	 * @param    string    $cipher
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCipher(string $cipher) : CryptoTraitInterface;
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCipher() : CryptoTraitInterface;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDigest() : string;
	
	/**
	 * @param    string    $digest
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDigest(string $digest) : CryptoTraitInterface;
	
	/**
	 * Check that digest is valid in current system
	 *
	 * @param    string    $digest
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkDigest(string $digest) : bool;
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDigest() : CryptoTraitInterface;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHash() : string;
	
	/**
	 * @param    string    $hash
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setHash(string $hash) : CryptoTraitInterface;
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHash() : CryptoTraitInterface;
	
	/**
	 * Get saved random bytes
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRandomBytes() : ?string;
	
	/**
	 * Set random bytes for later use
	 *
	 * @param    string    $bytes
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRandomBytes(string $bytes) : CryptoTraitInterface;
	
	/**
	 * Generates cryptographically secure pseudo-random bytes with given length
	 *
	 * @param    int    $length
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function generateRandomBytes(int $length) : string;
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRandomBytes() : CryptoTraitInterface;
	
	/**
	 * Hash the given data.
	 *
	 * @param    mixed    $value           The data to be hashed.
	 * @param    bool     $outputBinary    Specifies if the output should be in binary format.
	 *
	 * @return string The hashed data as a string (or binary).
	 * @since   3.0.0 First time introduced.
	 */
	public function hashData(mixed $value, bool $outputBinary) : string;
}
