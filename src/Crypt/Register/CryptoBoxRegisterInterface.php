<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Register;

//
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;

/**
 *
 */
interface CryptoBoxRegisterInterface extends RegisterInterface, RegisterPluginInterface {

}
