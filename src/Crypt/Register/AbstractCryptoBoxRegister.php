<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Register;

//
use Tiat\Stdlib\Register\AbstractRegister;

/**
 *
 */
abstract class AbstractCryptoBoxRegister extends AbstractRegister implements CryptoBoxRegisterInterface {

}
