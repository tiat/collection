<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Tiat\Collection\Crypt\Ext\CryptoBoxExtensionInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CryptoBoxInterface extends CryptoTraitInterface {
	
	/**
	 * Use openssl library and methods
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_EXTENSION_OPENSSL = CryptoBoxParams::CRYPTO_EXTENSION_OPENSSL;
	
	/**
	 * Use sodium library and methods
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_EXTENSION_SODIUM = CryptoBoxParams::CRYPTO_EXTENSION_SODIUM;
	
	/**
	 * Use custom library and methods
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_EXTENSION_CUSTOM = CryptoBoxParams::CRYPTO_EXTENSION_CUSTOM;
	
	/**
	 * Allowed extensions to manage cryptography
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const array CRYPTO_EXTENSION_ALLOWED = [self::CRYPTO_EXTENSION_OPENSSL, self::CRYPTO_EXTENSION_SODIUM,
	                                               self::CRYPTO_EXTENSION_CUSTOM];
	
	/**
	 * Password or shared secret key
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_MODE_SYMMETRIC = CryptoBoxParams::CRYPTO_MODE_SYMMETRIC;
	
	/**
	 * Public and private keypair
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_MODE_ASYMMETRIC = CryptoBoxParams::CRYPTO_MODE_ASYMMETRIC;
	
	/**
	 * Custom encryption algorithm process
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_MODE_CUSTOM = CryptoBoxParams::CRYPTO_MODE_CUSTOM;
	
	/**
	 * Allowed encryption algorithm categories
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const array CRYPTO_MODE_ALLOWED = [self::CRYPTO_MODE_SYMMETRIC, self::CRYPTO_MODE_ASYMMETRIC,
	                                          self::CRYPTO_MODE_CUSTOM];
	
	/**
	 * Default crypto extension (openssl)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_DEFAULT_EXTENSION = CryptoBoxParams::CRYPTO_EXTENSION_OPENSSL;
	
	/**
	 * Default encryption mode (asymmetric = private/public key)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_DEFAULT_MODE = CryptoBoxParams::CRYPTO_MODE_ASYMMETRIC;
	
	/**
	 * Acceptable PEM file types
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const array CRYPTO_PEM_FILE_ACCEPT = [CryptoBoxParams::CRYPTO_PEM_FILE_PRIVATE->name,
	                                             CryptoBoxParams::CRYPTO_PEM_FILE_PUBLIC->name];
	
	/**
	 * Private PEM file constant name for CryptoBox
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_PEM_FILE_PUBLIC = CryptoBoxParams::CRYPTO_PEM_FILE_PUBLIC;
	
	/**
	 * Private PEM file constant name for CryptoBox
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const CryptoBoxParams CRYPTO_PEM_FILE_PRIVATE = CryptoBoxParams::CRYPTO_PEM_FILE_PRIVATE;
	
	/**
	 * Default public PEM file name for CryptoBox
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_CRYPTO_PEM_FILE_PUBLIC = 'public.pem';
	
	/**
	 * Default private PEM file name for CryptoBox
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_CRYPTO_PEM_FILE_PRIVATE = 'private.pem';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string RANDOM_BYTES_KEY_IV = 'iv';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string RANDOM_BYTES_KEY_TAG = 'tag';
	
	/**
	 * Set public/private key PEM filename. Please set password for private key file if has been defined.
	 *
	 * @param    string             $filename
	 * @param    CryptoBoxParams    $fileType
	 * @param    null|string        $password    Optional or null if not set
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBoxPemFile(string $filename, CryptoBoxParams $fileType, string $password) : CryptoBoxInterface;
	
	/**
	 * @param    string    $path
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPath(string $path) : CryptoBoxInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCertPath() : ?string;
	
	/**
	 * Set password for certification(s)/encrypt/decrypt
	 *
	 * @param    string    $password
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoPassword(string $password) : CryptoBoxInterface;
	
	/**
	 * Reset password for certification(s)/encrypt/decrypt
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoPassword() : CryptoBoxInterface;
	
	/**
	 * Set options all at once
	 *
	 * @param    array    $options
	 * @param    bool     $overwrite
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOptions(array $options, bool $overwrite = FALSE) : CryptoBoxInterface;
	
	/**
	 * Get all options at once
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getOptions() : ?array;
	
	/**
	 * Reset all options at once
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetOptions() : CryptoBoxInterface;
	
	/**
	 * @param    string    $key
	 * @param    mixed     $value
	 * @param    bool      $overwrite
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOption(string $key, mixed $value, bool $overwrite = FALSE) : CryptoBoxInterface;
	
	/**
	 * @param    string        $key
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getOption(string $key, mixed $default) : mixed;
	
	/**
	 * Reset the option
	 *
	 * @param    string    $key
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetOption(string $key) : CryptoBoxInterface;
	
	/**
	 * Get CryptoBox extension library
	 *
	 * @return null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getExtension() : ?CryptoBoxParams;
	
	/**
	 * Get default CryptoBox extension (usually openssl)
	 *
	 * @return CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getExtensionDefault() : CryptoBoxParams;
	
	/**
	 * Define CryptoBox extension library
	 *
	 * @param    CryptoBoxParams    $value
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setExtension(CryptoBoxParams $value) : CryptoBoxInterface;
	
	/**
	 * Reset extension to default
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetExtension() : CryptoBoxInterface;
	
	/**
	 * Get CryptoBox extension object
	 *
	 * @return null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBoxExtension() : ?CryptoBoxExtensionInterface;
	
	/**
	 * Get CryptoBox extension interface
	 *
	 * @param    CryptoBoxParams    $value
	 *
	 * @return null|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBoxExtensionInterface(CryptoBoxParams $value) : ?CryptoBoxExtensionInterface;
	
	/**
	 * Set used CryptoBox extension object
	 *
	 * @param    CryptoBoxExtensionInterface    $interface
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBoxExtension(CryptoBoxExtensionInterface $interface) : CryptoBoxInterface;
	
	/**
	 * Reset used CryptoBox extension
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoBoxExtension() : CryptoBoxInterface;
	
	/**
	 * Get used CryptoBox encryption mode
	 *
	 * @return null|CryptoBoxParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMode() : ?CryptoBoxParams;
	
	/**
	 * Set used CryptoBox encryption mode
	 *
	 * @param    CryptoBoxParams    $param
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoMode(CryptoBoxParams $param) : CryptoBoxInterface;
	
	/**
	 * Reset used CryptoBox encryption mode
	 *
	 * @return CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoMode() : CryptoBoxInterface;
	
	/**
	 * Create signature.
	 * Private/public keys are optional. If you have already given those for CryptoBox extension then those will be used as default.
	 * Public key is needed if you want to verify signature after it has been generated.
	 *
	 * @param    string         $message
	 * @param    null|string    $keyPrivate    Give private key (optional)
	 * @param    null|string    $keyPublic     Give public key (if not set already) if you want to verify
	 * @param    bool           $verify        Verify the signature (optional, default FALSE)
	 *
	 * @return string|false
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, ?string $keyPrivate = NULL, ?string $keyPublic = NULL, bool $verify = FALSE) : string|false;
	
	/**
	 * Verify the signature
	 *
	 * @param    string    $message
	 * @param    string    $signature
	 * @param    mixed     $keyPublic
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function verifySignature(string $message, string $signature, mixed $keyPublic) : bool;
}
