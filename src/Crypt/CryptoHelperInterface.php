<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CryptoHelperInterface {
	
	/**
	 * Checks if a string is encoded with base64.
	 *
	 * @param    string    $data    The string to check.
	 *
	 * @return bool Returns true if the string is encoded with base64, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function isBase64Encoded(string $data) : bool;
	
	/**
	 * Checks if a value is serialized.
	 *
	 * @param    mixed    $value    The value to check.
	 *
	 * @return  bool     Returns true if the value is serialized, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function isSerialized(mixed $value) : bool;
}
