<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Tiat\Collection\Crypt\Ext\CryptoBoxExtensionInterface;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;

use function is_array;
use function is_string;

/**
 * Use symmetric method if you need to encrypt data that is stored in/to a database, for later retrieval and
 * by the same system. If you are encrypting large amounts of data such as a file, or any other case where only one
 * party needs to know the secret key. Otherwise, use asymmetric method with private/public keypair.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class CryptoBox extends AbstractCryptoBox {
	
	/**
	 * This is the fix where only one message can be decrypted. Now all random bytes are saved with message
	 * hash as a key and those can be retrieved using the same secret key and IV as an index.
	 *
	 * @var array
	 * @since
	 */
	private array $_cryptoMessageBytes;
	
	/**
	 * Open encrypted message
	 *
	 * @param    null|string              $data
	 * @param    null|array|string|int    $options
	 * @param    bool                     $base64    // Message is in base64 format
	 * @param    mixed                    ...$args
	 *
	 * @return CryptoBox
	 * @since   3.0.0 First time introduced.
	 */
	public function decrypt(?string $data = NULL, array|string|int|null $options = NULL, bool $base64 = FALSE, ...$args) : static {
		// Get&set TAG from args
		$tag = $args[self::RANDOM_BYTES_KEY_TAG] ?? NULL;
		$iv  = $args[self::RANDOM_BYTES_KEY_IV] ?? NULL;
		
		// Check if extension is available
		if(( $ext = $this->getCryptoBoxExtension() ) !== NULL):
			// Set password & tag
			$ext->setCryptPassword($this->_getCryptoPassword())->setCryptTag($tag);
			
			// Get IV from args
			$params = [self::RANDOM_BYTES_KEY_IV => $iv ?? $ext->getRandomBytes()];
			
			// Get encrypted message from extension
			$ext->decryptMessage($data ?? $this->getCryptoMessage(), 0, $base64, ...$params);
			
			// Get message from extension
			$this->setCryptoMessage($ext->getCryptoMessage());
		else:
			throw new RuntimeException("CryptoBox extension is not available.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Crypt the message
	 *
	 * @param    string              $message
	 * @param    array|string|int    $options
	 * @param    bool                $base64    Get encrypted message in base64 format (default: false)
	 *
	 * @return CryptoBox
	 * @since   3.0.0 First time introduced.
	 */
	public function encrypt(string $message, array|string|int $options = 0, bool $base64 = FALSE) : static {
		// Check if extension is available
		if(( $ext = $this->getCryptoBoxExtension() ) !== NULL && $ext instanceof CryptoTraitInterface):
			// Set password
			$ext->setCryptPassword($this->_getCryptoPassword());
			
			// Get encrypted message in base64 format
			if(( $msg = $ext->setCipher($this->getCipher())->encryptMessage($message, $options, $base64)
			                ->getCryptoMessage() ) !== NULL):
				// Save the message
				$this->setCryptoMessage($msg);
				
				// Save used arguments bytes
				if(( $bytes = $ext->getRandomBytes() ) !== NULL):
					// Get random bytes and tag
					$this->setRandomBytes($bytes);
					
					// Generate hash of message
					$hash = hash($this->getHash(), $msg);
					
					// This will allow only one message per instance (instance = CryptoBox instance)
					$this->_setCryptoMessageBytes($hash, self::RANDOM_BYTES_KEY_IV, $bytes);
					$this->_setCryptoMessageBytes($hash, self::RANDOM_BYTES_KEY_TAG, $ext->getCryptTag());
				endif;
			else:
				throw new RuntimeException("Can't encrypt the message for some reason. We don't know why.");
			endif;
		else:
			throw new RuntimeException("CryptoBox extension is missing.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMessageBytes() : ?array {
		return $this->_cryptoMessageBytes ?? NULL;
	}
	
	/**
	 * @param    string    $messageHash
	 * @param    string    $key
	 * @param    string    $value
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setCryptoMessageBytes(string $messageHash, string $key, string $value) : static {
		//
		$this->_cryptoMessageBytes[$messageHash][$key] = $value;
		
		return $this;
	}
	
	/**
	 * @param    string    $messageHash
	 * @param    string    $key
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMessageByte(string $messageHash, string $key) : ?string {
		return $this->_cryptoMessageBytes[$messageHash][$key] ?? NULL;
	}
	
	/**
	 * @param    string         $message
	 * @param    NULL|string    $keyPrivate
	 * @param    NULL|string    $keyPublic
	 * @param    bool           $verify
	 *
	 * @return string|false
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, ?string $keyPrivate = NULL, ?string $keyPublic = NULL, bool $verify = FALSE) : string|false {
		//
		if(( $ext = $this->getCryptoBoxExtension() ) !== NULL && $ext instanceof CryptoTraitInterface):
			// Check that private key exists (save system resources) & then try to generate signature
			if($this->_setKeyPrivate($ext, $keyPrivate) === TRUE && $ext->checkKeyPrivate($keyPrivate, TRUE) === TRUE &&
			   $ext->createSignature($message, $keyPrivate) === TRUE):
				// Check that public key has been set
				
				// Try to verify
				if(( $verify === TRUE ) &&
				   $this->verifySignature($message, $ext->getSignature(), $keyPublic ?? $ext->getKeyPublic()) ===
				   FALSE):
					// If signature check will return FALSE then return null
					return FALSE;
				endif;
				
				// Return the signature
				if(( $sign = $ext->getSignature() ) !== NULL):
					return $sign;
				endif;
			else:
				throw new InvalidArgumentException("Private key is missing and signature can't be created.");
			endif;
		else:
			throw new RuntimeException("CryptoBox extension is not available.");
		endif;
		
		// Return FALSE as default
		return FALSE;
	}
	
	/**
	 * @param    CryptoBoxExtensionInterface    $ext
	 * @param    NULL|string                    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	private function _setKeyPrivate(CryptoBoxExtensionInterface $ext, ?string $key = NULL) : bool {
		if($key !== NULL):
			return TRUE;
		elseif(! empty($pem = $this->_getCryptoBoxPemFile(CryptoBoxParams::CRYPTO_PEM_FILE_PRIVATE))):
			//
			if(is_array($pem)):
				$ext->readKeyPrivate($pem['filename'], $pem['passphrase']);
			elseif(is_string($pem)):
				$ext->readKeyPrivate($pem, $this->_getCryptoPassword());
			else:
				throw new RuntimeException("Can't read public key from file or string.");
			endif;
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    string         $message
	 * @param    null|string    $signature
	 * @param    mixed|NULL     $keyPublic
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function verifySignature(string $message, ?string $signature = NULL, mixed $keyPublic = NULL) : bool {
		//
		if(( $ext = $this->getCryptoBoxExtension() ) !== NULL && $ext instanceof CryptoTraitInterface):
			//
			if($this->_setKeyPublic($ext, $keyPublic) === TRUE):
				// Return signature verifying result
				return $ext->verifySignature($message, $signature ?? $ext->getSignature(), $keyPublic);
			else:
				throw new InvalidArgumentException("The public key hasn't been set.");
			endif;
		else:
			throw new RuntimeException("CryptoBox extension is not available.");
		endif;
	}
	
	/**
	 * @param    CryptoBoxExtensionInterface    $ext
	 * @param    NULL|string                    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	private function _setKeyPublic(CryptoBoxExtensionInterface $ext, ?string $key = NULL) : bool {
		//
		if($key !== NULL):
			return TRUE;
		elseif(! empty($pem = $this->_getCryptoBoxPemFile(CryptoBoxParams::CRYPTO_PEM_FILE_PUBLIC))):
			//
			if(is_array($pem)):
				$ext->readKeyPublic($pem['filename']);
			elseif(is_string($pem)):
				$ext->readKeyPublic($pem);
			else:
				throw new RuntimeException("Can't read public key from file or string.");
			endif;
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
}
