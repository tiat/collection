<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Tiat\Collection\Exception\InvalidArgumentException;

use function base64_decode;
use function base64_encode;
use function is_string;
use function preg_match;
use function trim;
use function unserialize;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait CryptoHelper {
	
	/**
	 * @param    string    $data
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBase64Encoded(string $data) : bool {
		return base64_encode(base64_decode($data, TRUE)) === $data;
	}
	
	/**
	 * @param    mixed    $value    The value to check.
	 *
	 * @return bool True if the value is serialized, false otherwise.
	 * @since 3.0.0 First time introduced.
	 */
	public function isSerialized(mixed $value) : bool {
		// If it isn't a string, it isn't serialized
		if(! is_string($value)):
			return FALSE;
		endif;
		
		// Trim the string to remove any extraneous whitespace
		// Check for empty string, which is not considered serialized
		if(empty($value = trim($value))):
			return FALSE;
		endif;
		
		// Check for basic serialized data structures
		if(preg_match('/^([adObis]):/', $value)):
			// Attempt to unserialize the data
			try {
				$unserialized = @unserialize($value, ["allowed_classes" => TRUE]);
				
				// If serialization returns false but data is not 'b:0;', return false
				return ! ( $unserialized === FALSE && $value !== 'b:0;' );
			} catch(InvalidArgumentException $e) {
				// Handle exceptions if any (though @unserialize() should suppress most warnings/errors)
				throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
			}
		endif;
		
		//
		return FALSE;
	}
}
