<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

use function sprintf;

/**
 * All available CryptoBox params
 * Use getDescription($value) to get description for each param
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum CryptoBoxParams: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * Define certification path in local filesystem
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_CERT_PATH = 'crypto_cert_path';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_MODE_SYMMETRIC = 'crypto_mode_encrypt_symmetric';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_MODE_ASYMMETRIC = 'crypto_mode_encrypt_asymmetric';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_MODE_CUSTOM = 'crypto_mode_encrypt_custom';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_EXTENSION_OPENSSL = 'crypto_extension_openssl';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_EXTENSION_SODIUM = 'crypto_extension_sodium';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_EXTENSION_CUSTOM = 'crypto_extension_custom';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_EXTENSION_PARAM_SETTINGS = 'crypto_extension_param_settings';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PARAM_EXTENSION = 'crypto_param_extension';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PARAM_MODE = 'crypto_param_mode';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PARAM_SETTINGS = 'crypto_param_settings';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PEM_FILE_PUBLIC = 'crypto_pem_file_public';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PEM_FILE_PRIVATE = 'crypto_pem_file_private';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PEM_KEY_PUBLIC = 'crypto_pem_key_public';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PEM_KEY_PRIVATE = 'crypto_pem_key_private';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_PEM_PASSWORD = 'crypto_pem_key_password';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_CIPHER = 'crypto_cipher';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_HASH = 'crypto_hash';
	
	/**
	 * @param    CryptoBoxParams    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDescription(CryptoBoxParams $value) : string {
		return match ( $value ) {
			self::CRYPTO_CIPHER => "Define used cipher, like aes-256-gcm",
			self::CRYPTO_HASH => "Define used hash like sha256",
			self::CRYPTO_MODE_SYMMETRIC => "Password or shared secret key",
			self::CRYPTO_MODE_ASYMMETRIC => "Public and private keypair",
			self::CRYPTO_MODE_CUSTOM => "Custom encryption algorithm process",
			self::CRYPTO_EXTENSION_OPENSSL => "Use openssl library and methods",
			self::CRYPTO_EXTENSION_SODIUM => "Use sodium library and methods",
			self::CRYPTO_EXTENSION_CUSTOM => "Use custom library and methods",
			self::CRYPTO_EXTENSION_PARAM_SETTINGS => "All custom settings for CryptoBox extension",
			self::CRYPTO_PARAM_EXTENSION => "Used extension for CryptoBox like openssl or sodium",
			self::CRYPTO_PARAM_MODE => "Used mode for CryptoBox like symmetric or asymmetric",
			self::CRYPTO_PARAM_SETTINGS => "All custom settings for CryptoBox",
			self::CRYPTO_PEM_FILE_PUBLIC, self::CRYPTO_PEM_FILE_PRIVATE => "PEM file name for CryptoBox",
			default => throw new InvalidArgumentException(sprintf("No description for value %s.", $value->value))
		};
	}
}
