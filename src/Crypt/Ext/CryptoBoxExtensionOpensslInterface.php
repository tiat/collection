<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CryptoBoxExtensionOpensslInterface extends CryptoBoxExtensionInterface {
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPrivateBits() : int;
	
	/**
	 * @param    int    $bits
	 *
	 * @return CryptoBoxExtensionOpensslInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivateBits(int $bits) : CryptoBoxExtensionOpensslInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPrivateType() : ?string;
	
	/**
	 * @param    string    $type
	 *
	 * @return CryptoBoxExtensionOpensslInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivateType(string $type) : CryptoBoxExtensionOpensslInterface;
}
