<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

//
use Tiat\Collection\Crypt\CryptoTraitInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CryptoBoxExtensionInterface extends CryptoTraitInterface {
	
	/**
	 * Get certificate & private/public key path
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCertPath() : ?string;
	
	/**
	 * Set certificate & private/public key path
	 *
	 * @param    string    $path
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPath(string $path) : CryptoBoxExtensionInterface;
	
	/**
	 * Reset certificate & private/public key path
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCertPath() : CryptoBoxExtensionInterface;
	
	/**
	 * Set password for certificate (file)
	 *
	 * @param    string    $password
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPassword(string $password) : CryptoBoxExtensionInterface;
	
	/**
	 * Reset certificate (file) password
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCertPassword() : CryptoBoxExtensionInterface;
	
	/**
	 * @param    string    $password
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptPassword(string $password) : CryptoBoxExtensionInterface;
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptPassword() : CryptoBoxExtensionInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptTag() : ?string;
	
	/**
	 * @param    string    $tag
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptTag(string $tag) : CryptoBoxExtensionInterface;
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptTag() : CryptoBoxExtensionInterface;
	
	/**
	 * @param    string    $cipher
	 *
	 * @return CryptoTraitInterface|CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCipher(string $cipher) : CryptoTraitInterface|CryptoBoxExtensionInterface;
	
	/**
	 * Decrypt the message. If $base64 is TRUE then data is encoded to base64.
	 *
	 * @param    string    $data
	 * @param    mixed     $options
	 * @param    bool      $base64    Is data base64 encoded
	 * @param    mixed     ...$args
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function decryptMessage(string $data, mixed $options, bool $base64, ...$args) : CryptoBoxExtensionInterface;
	
	/**
	 * Encrypt the message. If $base64 is TRUE then data is encoded to base64.
	 *
	 * @param    string        $message    Raw data
	 * @param    mixed|NULL    $options
	 * @param    bool          $base64     Encode data to base64
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function encryptMessage(string $message, mixed $options = NULL, bool $base64 = FALSE) : CryptoBoxExtensionInterface;
	
	/**
	 * Get public/private key details
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyDetails() : ?array;
	
	/**
	 * Set public/private key details at once as array. Define allowed keys is optional.
	 *
	 * @param    array         $details
	 * @param    bool          $overwrite    Overwrite the current key value if exists
	 * @param    NULL|array    $allowedKeys
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyDetailsAsArray(array $details, bool $overwrite = FALSE, ?array $allowedKeys = NULL) : CryptoBoxExtensionInterface;
	
	/**
	 * Set public/private key details. Define allowed keys is optional.
	 *
	 * @param    string|int    $key
	 * @param    mixed         $value
	 * @param    bool          $overwrite    Overwrite the current key value if exists
	 * @param    null|array    $allowedKeys
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyDetails(string|int $key, mixed $value, bool $overwrite = FALSE, ?array $allowedKeys = NULL) : CryptoBoxExtensionInterface;
	
	/**
	 * Get the public key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPublic() : mixed;
	
	/**
	 * Read public key from certificate pem file
	 *
	 * @param    string    $pem
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPublic(string $pem) : CryptoBoxExtensionInterface;
	
	/**
	 * Set the public key
	 *
	 * @param    mixed    $key
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPublic(mixed $key) : CryptoBoxExtensionInterface;
	
	/**
	 * Generate new public key from private key
	 *
	 * @param    mixed    $key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPublic(mixed $key) : mixed;
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPublic() : CryptoBoxExtensionInterface;
	
	/**
	 * Return if public key has a value (true if value exists)
	 * Notice! This will NOT check if public key is valid value
	 *
	 * @param    string    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPublic(string $key) : bool;
	
	/**
	 * Set private key
	 *
	 * @param    mixed    $key
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivate(mixed $key) : CryptoBoxExtensionInterface;
	
	/**
	 * Read private key from local file
	 *
	 * @param    string    $pem
	 * @param    string    $passphrase
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPrivate(string $pem, string $passphrase) : CryptoBoxExtensionInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPrivate() : mixed;
	
	/**
	 * Reset the private key
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPrivate() : CryptoBoxExtensionInterface;
	
	/**
	 * Return if private key has a value (true if value exists)
	 * Notice! This will NOT check if private key is valid value
	 *
	 * @param    mixed    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPrivate(mixed $key) : bool;
	
	/**
	 * Set signature
	 *
	 * @param    string    $signature
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSignature(string $signature) : CryptoBoxExtensionInterface;
	
	/**
	 * Get signature
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSignature() : ?string;
	
	/**
	 * Reset signature
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSignature() : CryptoBoxExtensionInterface;
	
	/**
	 * Create new signature from private key.
	 * You can get the signature with getSignature() if this method will return true.
	 *
	 * @param    string    $message
	 * @param    string    $keyPrivate    If key is provided it will replace system default
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, string $keyPrivate) : bool;
	
	/**
	 * Verify signature with public key
	 *
	 * @param    string    $message
	 * @param    string    $signature
	 * @param    string    $keyPublic    If key is provided it will replace system default
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function verifySignature(string $message, string $signature, string $keyPublic) : bool;
}
