<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

//
use Tiat\Collection\Crypt\CryptoHelper;
use Tiat\Collection\Crypt\CryptoHelperInterface;
use Tiat\Collection\Crypt\CryptoTrait;
use Tiat\Collection\Exception\InvalidArgumentException;

use function file_exists;
use function implode;
use function in_array;
use function is_dir;
use function is_readable;
use function register_shutdown_function;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractCryptoBoxBoxExtension implements CryptoBoxExtensionInterface, CryptoHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoHelper;
	
	/**
	 * For asymmetric method
	 *
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected mixed $_keyPrivate;
	
	/**
	 * For asymmetric method
	 *
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected mixed $_keyPublic;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_signature;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_keyDetails;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_certPath;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_certPassword;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptPassword;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptTag;
	
	/**
	 * @param    NULL|string    $certPath
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?string $certPath = NULL) {
		//
		register_shutdown_function([$this, 'shutdown']);
		
		//
		if(! empty($certPath)):
			$this->setCertPath($certPath);
		endif;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
	
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyDetails() : ?array {
		return $this->_keyDetails ?? NULL;
	}
	
	/**
	 * @param    string|int    $key
	 * @param    mixed         $value
	 * @param    bool          $overwrite
	 * @param    NULL|array    $allowedKeys
	 *
	 * @return AbstractCryptoBoxBoxExtension
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyDetails(string|int $key, mixed $value, bool $overwrite = FALSE, ?array $allowedKeys = NULL) : static {
		//
		if($allowedKeys !== NULL):
			//
			if(in_array($key, $allowedKeys, TRUE) === TRUE):
				$this->_setKeyDetails($key, $value, $overwrite);
			else:
				$msg = sprintf("The key '%s' is not allowed to set key details. Allowed keys are %s.", $key,
				               implode(', ', $allowedKeys));
				throw new InvalidArgumentException($msg);
			endif;
		else:
			$this->_setKeyDetails($key, $value, $overwrite);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    array         $details
	 * @param    bool          $overwrite
	 * @param    NULL|array    $allowedKeys
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyDetailsAsArray(array $details, bool $overwrite = FALSE, ?array $allowedKeys = NULL) : static {
		//
		if(! empty($details)):
			foreach($details as $key => $val):
				$this->setKeyDetails($key, $val, $overwrite, $allowedKeys);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCertPassword() : CryptoBoxExtensionInterface {
		//
		unset($this->_certPassword);
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptPassword() : CryptoBoxExtensionInterface {
		//
		unset($this->_cryptPassword);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptTag() : ?string {
		return $this->_cryptTag ?? NULL;
	}
	
	/**
	 * @param    string    $tag
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptTag(string $tag) : CryptoBoxExtensionInterface {
		//
		$this->_cryptTag = $tag;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptTag() : CryptoBoxExtensionInterface {
		//
		unset($this->_cryptTag);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCertPath() : ?string {
		return $this->_certPath ?? NULL;
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPath(string $path) : static {
		//
		if($path !== DIRECTORY_SEPARATOR && file_exists($path) && is_dir($path) && is_readable($path)):
			$this->_certPath = $path;
		else:
			$msg = sprintf("Given path (%s) doesn't exists or is not readable. Path can't be %s also.", $path,
			               DIRECTORY_SEPARATOR);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCertPath() : static {
		//
		unset($this->_certPath);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSignature() : ?string {
		return $this->_signature ?? NULL;
	}
	
	/**
	 * @param    string    $signature
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSignature(string $signature) : CryptoBoxExtensionInterface {
		//
		if(! empty($signature)):
			$this->_signature = $signature;
		else:
			throw new InvalidArgumentException("Signature can't be empty.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSignature() : CryptoBoxExtensionInterface {
		//
		unset($this->_signature);
		
		//
		return $this;
	}
	
	/**
	 * Get the private key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getKeyPrivate() : mixed;
	
	/**
	 * Get password for certificate (file)
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getCertPassword() : ?string {
		return $this->_certPassword ?? NULL;
	}
	
	/**
	 * @param    string    $password
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCertPassword(string $password) : CryptoBoxExtensionInterface {
		//
		$this->_certPassword = $password;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getCryptPassword() : ?string {
		return $this->_cryptPassword ?? NULL;
	}
	
	/**
	 * @param    string    $password
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptPassword(string $password) : CryptoBoxExtensionInterface {
		//
		$this->_cryptPassword = $password;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|int    $key
	 * @param    mixed         $value
	 * @param    bool          $overwrite
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	private function _setKeyDetails(string|int $key, mixed $value, bool $overwrite = FALSE) : static {
		// Don't throw exception if key value is sameU
		if(! isset($this->_keyDetails[$key]) || $overwrite === TRUE || $this->_keyDetails[$key] === $value):
			$this->_keyDetails[$key] = $value;
		else:
			$msg = sprintf("The key '%s' already exists in key details and overwrite is not allowed.", $key);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
}
