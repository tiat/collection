<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

//
use Exception;
use Laminas\Mime\Mime;
use OpenSSLAsymmetricKey;
use OpenSSLCertificate;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;

use function array_keys;
use function base64_decode;
use function file_exists;
use function file_get_contents;
use function get_debug_type;
use function implode;
use function in_array;
use function is_a;
use function is_array;
use function openssl_cipher_iv_length;
use function openssl_decrypt;
use function openssl_encrypt;
use function openssl_pkey_get_details;
use function openssl_pkey_get_private;
use function openssl_pkey_get_public;
use function openssl_pkey_new;
use function openssl_sign;
use function openssl_verify;
use function sprintf;
use function unserialize;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class CryptoBoxExtensionOpenssl extends AbstractCryptoBoxExtensionOpenssl {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	private const array KEY_DETAILS = ["bits" => "int", "key" => "string", "rsa" => "array", "dsa" => "array",
	                                   "dh" => "array", "type" => "int"];
	
	/**
	 * @param    string       $data
	 * @param    mixed        $options    Bitwise disjunction of the flags OPENSSL_RAW_DATA and OPENSSL_ZERO_PADDING
	 * @param    null|bool    $base64     Is data base64 encoded
	 * @param                 $args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function decryptMessage(string $data, mixed $options, ?bool $base64 = NULL, ...$args) : static {
		// Decode base64
		if($base64 === TRUE || ( $base64 === NULL && $this->isBase64Encoded($data) === TRUE )):
			$data = base64_decode($data, TRUE);
		endif;
		
		// Get & set named args
		$iv = $args['iv'] ?? '';
		
		//
		if($data !== FALSE):
			// Decrypt the message
			if(( $message = openssl_decrypt($data, $this->getCipher(), $this->_getCryptPassword(), $options, $iv,
			                                $this->getCryptTag()) ) === FALSE):
				// Decrypt failed
				throw new InvalidArgumentException("Decrypt has been failed! Please check that password, tag and used cipher are valid.");
			endif;
		else:
			throw new InvalidArgumentException("Data is not in base64 format or it contains character from outside the base64 alphabet.");
		endif;
		
		// Unserialize the message if serialized
		if($this->isSerialized($message)):
			$msg = unserialize($message, ['allowed_classes' => FALSE]);
		endif;
		
		//
		if(! empty($msg) || ! empty($message)):
			$this->setCryptoMessage($msg ?? $message);
		else:
			throw new InvalidArgumentException("Message don't have any content.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string        $message
	 * @param    mixed|NULL    $options    // Bitwise disjunction of the flags OPENSSL_RAW_DATA and OPENSSL_ZERO_PADDING
	 * @param    bool          $base64     // Return data as base64 encoded
	 *
	 * @return CryptoBoxExtensionOpenssl
	 * @since   3.0.0 First time introduced.
	 */
	public function encryptMessage(string $message, mixed $options = 0, bool $base64 = FALSE) : static {
		//
		if(( $length = openssl_cipher_iv_length($this->getCipher()) ) > 1):
			try {
				$this->setRandomBytes($this->generateRandomBytes($length));
			} catch(Exception $e) {
				throw new RuntimeException($e->getMessage());
			}
			
			// Encrypt the message
			$text = openssl_encrypt($message, $this->getCipher(), $this->_getCryptPassword(), $options,
			                        $this->getRandomBytes(), $tag);
			
			// Save authentication tag passed by reference
			$this->setCryptTag($tag);
			
			//
			if($base64 === TRUE):
				// Return as base64
				$this->setCryptoMessage(Mime::encodeBase64($text));
			else:
				// Return as plain text
				$this->setCryptoMessage($text);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $pem
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPublic(?string $pem = NULL) : static {
		//
		if(file_exists($pem)):
			if(( $key = file_get_contents($pem) ) !== FALSE):
				$this->setKeyPublic($key);
			else:
				$msg = sprintf("Given file (%s) doesn't include valid public key.", $pem);
				throw new InvalidArgumentException($msg);
			endif;
		elseif($pem !== NULL):
			$this->setKeyPublic($pem);
		else:
			$msg = sprintf("Given file %s doesn't exists", $pem);
			throw new InvalidArgumentException($msg);
		endif;
		
		return $this;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPublic(mixed $key) : static {
		//
		if($this->_checkKeyOpenssl(openssl_pkey_get_public($key), TRUE) !== FALSE):
			$this->_keyPublic = $key;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    $key
	 * @param    bool     $error    Throw an error if true and key is not a valid key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkKeyOpenssl(mixed $key, bool $error = FALSE) : bool {
		// Accept these values
		$accept = ['array', 'string'];
		$obj    = ['OpenSSLAsymmetricKey', 'OpenSSLCertificate'];
		
		//
		if(in_array(get_debug_type($key), $accept, TRUE)):
			return TRUE;
		else:
			foreach($obj as $val):
				if(is_a($key, $val, FALSE) === TRUE):
					return TRUE;
				endif;
			endforeach;
		endif;
		
		//
		if($error === TRUE):
			$msg = sprintf("Given type (%s) is not supported with openssl. It must be one of following: %s ",
			               get_debug_type($key), implode(", ", [...$accept, ...$obj]));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return false|OpenSSLAsymmetricKey
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPublic(mixed $key) : false|OpenSSLAsymmetricKey {
		//
		if($this->_checkKeyOpenssl($key, TRUE) !== FALSE && ( $details = openssl_pkey_get_details($key) ) !== FALSE):
			// Get & set the public key with details
			
			if(is_array($details) && ( $result = openssl_pkey_get_public($details['key'] ?? NULL) ) !== FALSE):
				// Set key details
				// Overwriting the current keys is ok if values are same (specially if you clone or use this same instance when handling both private/public keys)
				$this->setKeyDetailsAsArray($details, TRUE, array_keys(self::KEY_DETAILS));
				
				// Set the public key
				$this->setKeyPublic($result);
				
				//
				return $result;
			else:
				throw new InvalidArgumentException("Can't extract public key from certificate.");
			endif;
		else:
			throw new InvalidArgumentException("Can't get details from private key.");
		endif;
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPublic() : CryptoBoxExtensionInterface {
		//
		unset($this->_keyPublic);
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $pem
	 * @param    null|string    $passphrase
	 *
	 * @return CryptoBoxExtensionOpenssl
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPrivate(?string $pem = NULL, ?string $passphrase = NULL) : static {
		//
		if(! empty($pem)):
			if(file_exists($pem)):
				if(( $key = file_get_contents($pem) ) !== FALSE):
					$this->setKeyPrivate($key, $passphrase ?? $this->_getCertPassword());
				else:
					$msg = sprintf("Given file (%s) doesn't include valid private key.", $pem);
					throw new InvalidArgumentException($msg);
				endif;
			else:
				$this->setKeyPrivate($pem, $passphrase ?? $this->_getCertPassword());
			endif;
		else:
			$msg = sprintf("Given file %s doesn't exists", $pem);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed          $key
	 * @param    null|string    $passphrase
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivate(mixed $key, ?string $passphrase = NULL) : static {
		//
		if($this->_checkKeyOpenssl($result = openssl_pkey_get_private($key, $passphrase), TRUE) !== FALSE):
			$this->_keyPrivate = $result;
		else:
			$msg = sprintf("Private key must be an instance of string|array|%s|%s", OpenSSLAsymmetricKey::class,
			               OpenSSLCertificate::class);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|array    $options
	 *
	 * @return OpenSSLAsymmetricKey|false
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPrivate(?array $options = NULL) : OpenSSLAsymmetricKey|false {
		// Define private key with options
		$config = ["digest_alg" => $this->getDigest(), "private_key_bits" => $this->getKeyPrivateBits(),
		           "private_key_type" => $this->getKeyPrivateType(),];
		
		//
		if(( $key = openssl_pkey_new($config) ) !== FALSE):
			// Collect & set key details
			$this->setKeyDetailsAsArray(openssl_pkey_get_details($key), TRUE, array_keys(self::KEY_DETAILS));
			
			//
			return $key;
		endif;
		
		//
		throw new RuntimeException("There is an error when creating new private key. Maybe your openssl.cnf is missing?");
	}
	
	/**
	 * @return CryptoBoxExtensionOpenssl
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPrivate() : static {
		//
		unset($this->_keyPrivate);
		
		//
		return $this;
	}
	
	/**
	 * @param    string         $message
	 * @param    null|string    $keyPrivate
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, ?string $keyPrivate = NULL) : bool {
		//
		if(( ( $this->checkKeyPrivate($key = $keyPrivate ?? $this->_getKeyPrivate(), TRUE) ) === TRUE ) &&
		   ( $this->checkDigest($digest = $this->getDigest()) === TRUE )):
			// Try to generate the signature
			if(openssl_sign($message, $signature, $key, $digest) === TRUE):
				//
				$this->setSignature($signature);
				
				//
				return TRUE;
			else:
				throw new RuntimeException("Can't generate signature for the message.");
			endif;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    mixed|NULL    $key
	 * @param    bool          $error    Throw an error if true and key is not a valid key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPrivate(mixed $key = NULL, bool $error = FALSE) : bool {
		return $this->_checkKeyOpenssl($key ?? $this->_getKeyPrivate(), $error);
	}
	
	/**
	 * @return null|string|false|OpenSSLAsymmetricKey
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getKeyPrivate() : null|string|false|OpenSSLAsymmetricKey {
		return $this->_keyPrivate ?? NULL;
	}
	
	/**
	 * @param    string         $message
	 * @param    NULL|string    $signature
	 * @param    NULL|string    $keyPublic
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function verifySignature(string $message, ?string $signature = NULL, ?string $keyPublic = NULL) : bool {
		// Check that signature exists
		if(empty($signature = $signature ?? $this->getSignature())):
			throw new InvalidArgumentException("Signature can't be empty.");
		endif;
		
		// Check that public key is ok
		if($this->checkKeyPublic($keyPublic = (string)( $keyPublic ?? $this->getKeyPublic() )) === FALSE):
			throw new InvalidArgumentException("Public key can't be empty.");
		endif;
		
		//
		if($this->checkDigest($digest = $this->getDigest()) === TRUE):
			// Try to verify the signature
			$result = openssl_verify($message, $signature, $keyPublic, $digest);
			
			//
			return match ( $result ) {
				1 => TRUE,
				0 => FALSE,
				default => throw new RuntimeException(sprintf("Error while checking signature (%s).", __METHOD__))
			};
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    mixed|NULL    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPublic(mixed $key = NULL) : bool {
		return $this->_checkKeyOpenssl($key ?? $this->getKeyPublic());
	}
	
	/**
	 * @param    NULL|string    $key
	 *
	 * @return null|string|false|OpenSSLAsymmetricKey
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPublic(?string $key = NULL) : null|string|false|OpenSSLAsymmetricKey {
		return $this->_keyPublic ?? NULL;
	}
}
