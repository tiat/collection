<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

//
use Exception;
use SodiumException;
use Tiat\Collection\Exception\RuntimeException;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class CryptoBoxExtensionSodium extends AbstractCryptoBoxBoxExtension {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nonce;
	
	/**
	 * X25519 secret/public key
	 *
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private readonly string $_keyPair;
	
	public function decryptMessage(string $data, mixed $options, bool $base64, ...$args) : static {
		#string $data, string $iv, bool $base64 = FALSE, mixed $options = 0
		return $this;
	}
	
	/**
	 * @param    string        $message
	 * @param    mixed|NULL    $options
	 * @param    bool          $base64
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function encryptMessage(string $message, mixed $options = NULL, bool $base64 = FALSE) : static {
		// TODO: Implement encryptMessage() method.
		return $this;
	}
	
	/**
	 * @param    NULL|string    $key
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPublic(?string $key = NULL) : string {
		//
		if(empty($this->_keyPublic)):
			try {
				$this->_keyPublic = sodium_crypto_box_publickey($this->_getKeyPair());
			} catch(SodiumException $e) {
				throw new RuntimeException($e->getMessage());
			}
		endif;
		
		//
		return $this->_keyPublic;
	}
	
	/**
	 * Randomly generate a secret key and a corresponding public key
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getKeyPair() : string {
		if(empty($this->_keyPair)):
			try {
				$this->_keyPair = sodium_crypto_box_keypair();
			} catch(SodiumException $e) {
				throw new RuntimeException($e->getMessage());
			}
		endif;
		
		//
		return $this->_keyPair;
	}
	
	/**
	 * @param    NULL|string    $key
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPrivate(?string $key = NULL) : string {
		//
		if(empty($this->_keyPrivate)):
			try {
				$this->_keyPrivate = sodium_crypto_box_secretkey($this->_getKeyPair());
			} catch(SodiumException $e) {
				throw new RuntimeException($e->getMessage());
			}
		endif;
		
		//
		return $this->_keyPrivate;
	}
	
	/**
	 * @param    int    $length
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function _generateRandomBytes(int $length = SODIUM_CRYPTO_SECRETBOX_NONCEBYTES) : static {
		try {
			$this->setNonce($this->generateRandomBytes($length));
		} catch(Exception $e) {
			throw new RuntimeException($e->getMessage());
		}
		
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getNonce() : ?string {
		return $this->_nonce ?? NULL;
	}
	
	/**
	 * @param    string    $nonce
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setNonce(string $nonce) : static {
		//
		$this->_nonce = $nonce;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPublic(mixed $key) : CryptoBoxExtensionInterface {
		// TODO: Implement setKeyPublic() method.
	}
	
	/**
	 * @param    mixed    $key    *
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPublic(mixed $key) : mixed {
		// TODO: Implement generateKeyPublic() method.
	}
	
	/**
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPublic() : CryptoBoxExtensionInterface {
		// TODO: Implement resetKeyPublic() method.
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivate(mixed $key) : CryptoBoxExtensionInterface {
		// TODO: Implement setKeyPrivate() method.
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function generateKeyPrivate() : mixed {
		// TODO: Implement generateKeyPrivate() method.
	}
	
	/**
	 * @return CryptoBoxExtensionSodium
	 * @since   3.0.0 First time introduced.
	 */
	public function resetKeyPrivate() : static {
		// TODO: Implement resetKeyPrivate() method.
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPublic(string $key = NULL) : bool {
		// TODO: Implement checkKeyPublic() method.
		
		//
		return FALSE;
	}
	
	/**
	 * @param    string    $pem
	 * @param    string    $passphrase    *
	 *
	 * @return CryptoBoxExtensionSodium
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPrivate(string $pem, string $passphrase) : static {
		// TODO: Implement readKeyPrivate() method.
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    $key
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkKeyPrivate(mixed $key) : bool {
		// TODO: Implement checkKeyPrivate() method.
		
		//
		return FALSE;
	}
	
	/**
	 * @param    string         $message
	 * @param    null|string    $keyPrivate    *
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, string $keyPrivate = NULL) : bool {
		// TODO: Implement createSignature() method.
		
		//
		return FALSE;
	}
	
	/**
	 * @param    string    $certFile
	 *
	 * @return CryptoBoxExtensionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function readKeyPublic(string $certFile) : CryptoBoxExtensionInterface {
		// TODO: Implement readKeyPublic() method.
	}
	
	/**
	 * @param    string         $message
	 * @param    NULL|string    $signature
	 * @param    NULL|string    $keyPublic
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function verifySignature(string $message, ?string $signature = NULL, ?string $keyPublic = NULL) : bool {
		// TODO: Implement verifySignature() method.
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getKeyPrivate() : mixed {
		// TODO: Implement _getKeyPrivate() method.
	}
}
