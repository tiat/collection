<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt\Ext;

//
use OpenSSLAsymmetricKey;
use Tiat\Collection\Crypt\CryptoHelper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractCryptoBoxExtensionOpenssl extends AbstractCryptoBoxBoxExtension implements CryptoBoxExtensionOpensslInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int DEFAULT_PRIVATE_KEY_BITS = 2048;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_PRIVATE_KEY_TYPE = 'OPENSSL_KEYTYPE_RSA';
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_keyPrivateBits = self::DEFAULT_PRIVATE_KEY_BITS;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_keyPrivateType = self::DEFAULT_PRIVATE_KEY_TYPE;
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPrivateBits() : int {
		return $this->_keyPrivateBits;
	}
	
	/**
	 * @param    int    $bits
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivateBits(int $bits) : static {
		//
		$this->_keyPrivateBits = $bits;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getKeyPrivateType() : ?string {
		return $this->_keyPrivateType ?? NULL;
	}
	
	/**
	 * @param    string    $type
	 *
	 * @return CryptoBoxExtensionOpensslInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setKeyPrivateType(string $type) : CryptoBoxExtensionOpensslInterface {
		//
		$this->_keyPrivateType = $type;
		
		//
		return $this;
	}
	
	/**
	 * Check that given key is string|array|OpenSSLAsymmetricKey|OpenSSLCertificate
	 *
	 * @param    mixed    $key
	 * @param    bool     $error
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _checkKeyOpenssl(mixed $key, bool $error = FALSE) : bool;
	
	/**
	 * @return null|string|false|OpenSSLAsymmetricKey
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getKeyPrivate() : null|string|false|OpenSSLAsymmetricKey;
}
