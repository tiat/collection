<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Crypt;

//
use Exception;
use Random\RandomException;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;

use function hash;
use function hash_hmac_algos;
use function implode;
use function in_array;
use function is_scalar;
use function openssl_get_cipher_methods;
use function openssl_get_md_methods;
use function random_bytes;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait CryptoTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_CIPHER = 'aes-256-gcm';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_DIGEST = 'sha256';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_HASH = 'sha256';
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoCipher = self::DEFAULT_CIPHER;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoDigest = self::DEFAULT_DIGEST;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoHash = self::DEFAULT_HASH;
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_randomBytes;
	
	/**
	 * Message from/to encrypt/decrypt
	 *
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_cryptoMessage;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoMessage() : mixed {
		return $this->_cryptoMessage ?? NULL;
	}
	
	/**
	 * @param    mixed    $message
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoMessage(mixed $message) : CryptoTraitInterface {
		//
		if(! empty($message)):
			$this->_cryptoMessage = $message;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoMessage() : CryptoTraitInterface {
		//
		unset($this->_cryptoMessage);
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getCipher() : string {
		return $this->_cryptoCipher;
	}
	
	/**
	 * Set cipher
	 * Notice! Only openssl cipher methods allowed
	 *
	 * @param    string    $cipher
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCipher(string $cipher) : CryptoTraitInterface {
		// Check that given cipher is valid & accept also aliases (like aes256)
		if(in_array(strtolower($cipher), openssl_get_cipher_methods(TRUE), TRUE) === TRUE):
			$this->_cryptoCipher = $cipher;
		else:
			$msg = sprintf("Given cipher (%s) is not supported. Please use one of following: %s.", $cipher,
			               implode(', ', openssl_get_cipher_methods(TRUE)));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Reset used cipher method to default
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCipher() : CryptoTraitInterface {
		//
		$this->_cryptoCipher = self::DEFAULT_CIPHER;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDigest() : string {
		return $this->_cryptoDigest;
	}
	
	/**
	 * Set digest method
	 * Notice! Only openssl digest methods allowed
	 *
	 * @param    string    $digest
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDigest(string $digest) : CryptoTraitInterface {
		//
		if($this->checkDigest($digest) === TRUE):
			$this->_cryptoDigest = $digest;
		else:
			$msg = sprintf("Given digest (%s) is not supported. Please use one of following: %s.", $digest,
			               implode(', ', openssl_get_md_methods(TRUE)));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $digest
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkDigest(string $digest) : bool {
		if(in_array(strtolower($digest), openssl_get_md_methods(TRUE), TRUE) === TRUE):
			return TRUE;
		endif;
		
		//
		$msg = sprintf("Digest (%s) is not valid in this system.", $digest);
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * Reset used digest method to default
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDigest() : CryptoTraitInterface {
		//
		$this->_cryptoDigest = self::DEFAULT_DIGEST;
		
		//
		return $this;
	}
	
	/**
	 * Set hash method
	 * Notice! Only in system registered/supported hashing algorithms allowed
	 *
	 * @param    string    $hash
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setHash(string $hash) : CryptoTraitInterface {
		//
		if(in_array(strtolower($hash), hash_hmac_algos(), TRUE)):
			$this->_cryptoHash = $hash;
		else:
			$msg = sprintf("Given hashing algo (%s) is not supported. Please use one of following: %s.", $hash,
			               implode(', ', hash_hmac_algos()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Reset used hash method to default
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHash() : CryptoTraitInterface {
		//
		$this->_cryptoHash = self::DEFAULT_HASH;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRandomBytes() : ?string {
		return $this->_randomBytes ?? NULL;
	}
	
	/**
	 * @param    string    $bytes
	 *
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRandomBytes(string $bytes) : CryptoTraitInterface {
		//
		if($bytes !== ''):
			$this->_randomBytes = $bytes;
		else:
			throw new InvalidArgumentException("Bytes must have at least 1 character");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Generate random bytes
	 *
	 * @param    int    $length
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function generateRandomBytes(int $length) : string {
		try {
			if($length > 0):
				return random_bytes($length);
			else:
				$msg = sprintf("Length must be >= 1, %u given", $length);
				throw new InvalidArgumentException($msg);
			endif;
		} catch(Exception|RandomException $e) {
			throw new RuntimeException($e->getMessage());
		}
	}
	
	/**
	 * @return CryptoTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRandomBytes() : CryptoTraitInterface {
		//
		unset($this->_randomBytes);
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    $value
	 * @param    bool     $outputBinary
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function hashData(mixed $value, bool $outputBinary = FALSE) : string {
		// Check if value is scalar and able to hash, else throw exception
		// Let user decide that what to do with non-scalar values
		if(is_scalar($value) === TRUE):
			return hash($this->getHash(), (string)$value, $outputBinary);
		else:
			throw new InvalidArgumentException("Value must be a scalar/string.");
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHash() : string {
		return $this->_cryptoHash;
	}
}
