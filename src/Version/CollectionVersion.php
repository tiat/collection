<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Version;

//
use Jantia\Plugin\Monitor\Std\MetricsFactor;
use Jantia\Stdlib\Version\AbstractVersionPackage;

use function extension_loaded;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class CollectionVersion extends AbstractVersionPackage {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string PACKAGE_VERSION_NAME = 'tiat/collection';
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMetrics() : ?array {
		return [MetricsFactor::class => [MetricsFactor::RELIABILITY, MetricsFactor::PERFORMANCE]];
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function systemCheck() : bool {
		return ( extension_loaded('openssl') && extension_loaded('sodium') );
	}
}
