<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope;

//
use Tiat\Collection\Crypt\CryptoBoxInterface;
use Tiat\Collection\Crypt\CryptoBoxParams;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;
use Tiat\Config\Config;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Register\Register;

use function class_exists;
use function is_string;
use function serialize;
use function sprintf;

/**
 * @TODO @FIXME: This class is not production-ready.
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Envelope extends AbstractEnvelope {
	
	/**
	 * @param    array                   $values
	 * @param    null|ConfigInterface    $_config
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(array $values = [], ?ConfigInterface $_config = NULL) {
		//
		parent::__construct($values, $_config);
		
		// Set module config
		if(( ( $c = $this->_config ) !== NULL ) &&
		   $c->get(EnvelopeParams::ENVELOPE_PARAM_STATUS_CRYPT->value) === TRUE):
			echo 'Try fetch...<br>';
			var_dump($c->get(CryptoBoxParams::CRYPTO_EXTENSION_PARAM_SETTINGS->value));
			echo '<br><br>';
			
			// Get extension settings
			$params = new Config($c->get(CryptoBoxParams::CRYPTO_EXTENSION_PARAM_SETTINGS->value) ?? []);
			echo '<br><br>';
			echo 'Test settings<br>';
			var_dump($params);
			echo '<br><br>';
			
			echo 'Try search register with name...<br>';
			if(( $reg = Register::getRegister('cryptoboxRegister') ) !== NULL &&
			   $reg instanceof RegisterPluginInterface):
				echo 'Get test config value: <br>';
				$test = $reg->getConfig(CryptoBoxParams::CRYPTO_PARAM_EXTENSION);
				var_dump($test);
				echo '<br><br><br>';
			else:
				throw new RuntimeException("Can't open CryptoBox Register.");
			endif;
			
			// Get the extension for CryptoBox
			if(( ( $ext = $c->get(EnvelopeParams::ENVELOPE_PARAM_CRYPTO_EXTENSION->value) ) !== NULL ) &&
			   is_string($ext) && class_exists($ext, TRUE)):
				// Declare new object from string
				$cr = new $ext();
			endif;
			
			// Set extension if defined
			if(isset($cr) && $cr instanceof CryptoBoxInterface):
				// Define CryptoBox extension: openssl/sodium or custom
				if(( $value = $c->get(CryptoBoxParams::CRYPTO_PARAM_EXTENSION->value) ) !== NULL):
					$cr->setExtension(CryptoBoxParams::toValue($value));
				endif;
				
				// Set crypt mode if defined: symmetric or asymmetric
				if(( $mode = $c->get(CryptoBoxParams::CRYPTO_PARAM_MODE->value) ) !== NULL):
					$cr->setCryptoMode(CryptoBoxParams::toValue($mode));
				endif;
				
				// Register CryptoBox interface
				$this->setCryptoBox($cr);
			else:
				$msg = sprintf("Extension is not defined or it's not an instance of %s.", CryptoBoxInterface::class);
				throw new InvalidArgumentException($msg);
			endif;
		endif;
	}
	
	/**
	 * Open crypted message
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function open() : static {
		return $this;
	}
	
	/**
	 * @param    NULL|string    $message
	 * @param    bool           $signature    Create signature (default: false)
	 * @param    bool           $base64       Return in base64 format (default: false)
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function close(?string $message = NULL, bool $signature = FALSE, bool $base64 = FALSE) : static {
		echo 'Method: ' . __METHOD__ . '<br>';
		
		if(( $c = $this->getCryptoBox() ) !== NULL):
			echo 'Convert DTO to array and serialize<br>';
			
			// Use DTO if message is empty
			if($message === NULL && empty($dto = $this->getDto()?->toString())):
				throw new InvalidArgumentException("Either message or DTO must exists");
			else:
				//
				if(( $content = $message ) === NULL && ! empty($dto)):
					$content = serialize($dto);
				endif;
				
				// Set password for CryptoBox
				$c->setCryptoPassword('MyTestPasswordForCryptoBox');
				
				// Set PEM file(s) for CryptoBox
				// @TODO @FIXME This is @dev line
				$c->setCryptoBoxPemFile('apislash_private.pem', CryptoBoxParams::CRYPTO_PEM_FILE_PRIVATE, 'SA12la34!!')
				  ->setCryptoBoxPemFile('apislash_public.pem', CryptoBoxParams::CRYPTO_PEM_FILE_PUBLIC);
				
				//
				if(( $msg = $c->encrypt(message:$content, base64:$base64) ) !== NULL &&
				   $msg instanceof EnvelopeInterface):
					// Create signature for the Envelope & verify it
					$this->setMessage($msg->getMessage());
					
					// Signature the message if required
					if($signature === TRUE && ( $signed = $this->createSignature($msg->getMessage(), TRUE) ) !== FALSE):
						$this->setSignature($signed);
					endif;
					
					// @TODO @FIXME
					echo 'Envelope config:<br>';
					var_dump($this->_config);
					echo '<br><br>';
					echo '<br><br>';
					
					echo '<br><br>';
					echo 'Message: ' . $this->getMessage() . '<br>';
					echo '<br><br>';
					echo 'Signature: ' . bin2hex($this->createSignature($msg->getMessage(), TRUE)) . '<br>';
					echo '<br><br>';
					echo 'Get all used options<br>';
					var_dump($msg->getOptions());
					echo '<br><br>';
					
					if(1 === 2):
						echo 'Show encrypted message: ' . $msg->getMessage() . '<br>';
						echo 'Try encrypt the message with new instance<br>';
						
						$certPath = '/data/web/pool/certs/';
						$class    = self::DEFAULT_CRYPTO;
						$test     = new $class($certPath);
						$test->setCryptoPassword('MyTestPasswordForCryptoBox');
						$args = ['tag' => $c->getOption('tag')];
						#var_dump($args);
						$test->decrypt($msg->getMessage(), $c->getRandomBytes(), $base64, ...$args);
						
						echo 'Decrypted message: <br>';
						$te = $test->getMessage();
						var_dump($te);
						echo '<br><br>';
					endif;
					
					echo '<br><br>End of Envelope<br><br>';
					
					return $this;
				endif;
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $message
	 * @param    bool      $verify
	 *
	 * @return false|string
	 * @since   3.0.0 First time introduced.
	 */
	public function createSignature(string $message, bool $verify = FALSE) : false|string {
		if(( $c = $this->getCryptoBox() ) !== NULL):
			return $c->createSignature($message, verify:$verify);
		endif;
		
		//
		throw new RuntimeException("Can't get CryptoBox extension interface.");
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _closeSymmetric() {
	
	}
}
