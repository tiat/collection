<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope;

//
use Tiat\Collection\Crypt\CryptoBox;
use Tiat\Collection\Crypt\CryptoBoxInterface;
use Tiat\Collection\Dto\DataTransferObjectInterface;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Stdlib\Parameters\Parameters;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractEnvelope extends Parameters implements EnvelopeInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_CRYPTO = CryptoBox::class;
	
	/**
	 * @var DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	private DataTransferObjectInterface $_dto;
	
	/**
	 * @var CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	private CryptoBoxInterface $_cryptoBoxInterface;
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_cryptoBoxMessage;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_cryptoBoxSignature;
	
	/**
	 * @param    array    $values
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function __invoke(array $values) : static {
		//
		$this->__construct($values);
		
		//
		return $this;
	}
	
	/**
	 * @param    array                   $values
	 * @param    null|ConfigInterface    $_config
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(array $values = [], protected ?ConfigInterface $_config = NULL) {
		parent::__construct($values);
	}
	
	/**
	 * @return null|DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDto() : ?DataTransferObjectInterface {
		return $this->_dto ?? NULL;
	}
	
	/**
	 * @param    DataTransferObjectInterface    $dto
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDto(DataTransferObjectInterface $dto) : static {
		//
		$this->_dto = $dto;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDto() : static {
		//
		unset($this->_dto);
		
		//
		return $this;
	}
	
	/**
	 * @param    bool     $useDefault
	 * @param    mixed    ...$args
	 *
	 * @return null|CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBox(bool $useDefault = FALSE, ...$args) : ?CryptoBoxInterface {
		//
		if($useDefault === TRUE):
			// @TODO @FIXME
			$certPath = '/data/web/pool/certs/';
			$class    = self::DEFAULT_CRYPTO;
			$this->setCryptoBox(new $class($certPath));
		endif;
		
		//
		return $this->_cryptoBoxInterface ?? NULL;
	}
	
	/**
	 * @param    CryptoBoxInterface    $interface
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBox(CryptoBoxInterface $interface) : EnvelopeInterface {
		//
		$this->_cryptoBoxInterface = $interface;
		
		//
		return $this;
	}
	
	/**
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoBox() : EnvelopeInterface {
		//
		unset($this->_cryptoBoxInterface);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessage() : ?string {
		return $this->_cryptoBoxMessage ?? NULL;
	}
	
	/**
	 * @param    string    $msg
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(string $msg) : static {
		//
		if(! empty($msg)):
			$this->_cryptoBoxMessage = $msg;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessage() : static {
		//
		unset($this->_cryptoBoxMessage);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $signature
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setSignature(string $signature) : static {
		//
		if(! empty($signature)):
			$this->_cryptoBoxSignature = $signature;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSignature() : ?string {
		return $this->_cryptoBoxSignature ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSignature() : static {
		//
		unset($this->_cryptoBoxSignature);
		
		//
		return $this;
	}
}
