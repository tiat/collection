<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope\Register;

//
use Tiat\Stdlib\Register\RegisterHelperInterface;
use Tiat\Stdlib\Register\RegisterHelperTrait;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EnvelopeRegister extends AbstractEnvelopeRegister implements RegisterHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RegisterHelperTrait;
}
