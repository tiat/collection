<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope\Register;

//

use Tiat\Stdlib\Register\AbstractRegister;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractEnvelopeRegister extends AbstractRegister {

}
