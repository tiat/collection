<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope;

//
use Tiat\Collection\Crypt\CryptoBoxInterface;
use Tiat\Collection\Dto\DataTransferObjectInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface EnvelopeInterface {
	
	/**
	 * @param    DataTransferObjectInterface    $dto
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDto(DataTransferObjectInterface $dto) : EnvelopeInterface;
	
	/**
	 * @return null|DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDto() : ?DataTransferObjectInterface;
	
	/**
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDto() : EnvelopeInterface;
	
	/**
	 * Get CryptoBox Interface (default CryptoBox)
	 *
	 * @return null|CryptoBoxInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getCryptoBox() : ?CryptoBoxInterface;
	
	/**
	 * Set CryptoBox Interface (default CryptoBox)
	 *
	 * @param    CryptoBoxInterface    $interface
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCryptoBox(CryptoBoxInterface $interface) : EnvelopeInterface;
	
	/**
	 * Reset CryptoBox Interface
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetCryptoBox() : EnvelopeInterface;
	
	/**
	 * Get encrypted CryptoBox message
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessage() : ?string;
	
	/**
	 * Set encrypted CryptoBox message
	 *
	 * @param    string    $msg
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(string $msg) : EnvelopeInterface;
	
	/**
	 * Reset CryptoBox message
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessage() : EnvelopeInterface;
	
	/**
	 * Set the signature
	 *
	 * @param    string    $signature
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSignature(string $signature) : EnvelopeInterface;
	
	/**
	 * Get signature
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSignature() : ?string;
	
	/**
	 * Reset the signature
	 *
	 * @return EnvelopeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetSignature() : EnvelopeInterface;
}
