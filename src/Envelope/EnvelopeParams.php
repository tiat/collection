<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Envelope;

//
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

use function sprintf;

/**
 * Params for Envelope module
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum EnvelopeParams: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENVELOPE_PARAM_CRYPTO_EXTENSION = 'envelope_param_crypto_extension';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENVELOPE_PARAM_STATUS_CRYPT = 'envelope_param_status';
	
	/**
	 * @param    EnvelopeParams    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDescription(EnvelopeParams $value) : string {
		return match ( $value ) {
			self::ENVELOPE_PARAM_CRYPTO_EXTENSION => "What extension is used with Envelope crypt",
			self::ENVELOPE_PARAM_STATUS_CRYPT => "Set envelope crypt ON or OFF",
			default => throw new InvalidArgumentException(sprintf("No description for value %s", $value->value))
		};
	}
}
