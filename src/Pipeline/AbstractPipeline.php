<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Pipeline;

//
use Generator;
use Jantia\Asi\Helper\AsiHelperInterfaceTrait;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Pipeline\PipelineInterface;
use Tiat\Standard\Pipeline\PipelineVars;
use Tiat\Stdlib\Loader\ClassLoaderTrait;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function implode;
use function in_array;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractPipeline implements ClassLoaderInterface, ParametersPluginInterface, PipelineInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiHelperInterfaceTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array PIPE_ACTIONS_DEFAULT = [PipelineVars::PIPE_ACTION_AFTER->value,
	                                                 PipelineVars::PIPE_ACTION_ARGS->value,
	                                                 PipelineVars::PIPE_ACTION_BEFORE->value,
	                                                 PipelineVars::PIPE_ACTION_FIBER->value,
	                                                 PipelineVars::PIPE_ACTION_METHOD->value,
	                                                 PipelineVars::PIPE_ACTION_NAME->value,
	                                                 PipelineVars::PIPE_ACTION_RESULT->value,
	                                                 PipelineVars::PIPE_ACTION_RESULT_AFTER->value,
	                                                 PipelineVars::PIPE_ACTION_RESULT_BEFORE->value,
	                                                 PipelineVars::PIPE_ACTION_STATUS->value,
	                                                 PipelineVars::PIPE_ACTION_SUSPEND->value];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_pipeActions;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = FALSE, bool $autorun = FALSE) {
		//
		$this->setParams($params);
		
		//
		$this->setAutoInit($autoinit);
		
		//
		$this->setAutoRun($autorun);
		
		//
		$this->registerPipeActions(self::PIPE_ACTIONS_DEFAULT);
	}
	
	/**
	 * @param    array    $actions
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipeActions(array $actions) : static {
		//
		if(empty($this->_pipeActions)):
			$this->_pipeActions = $actions;
		else:
			$this->_pipeActions = [...$this->_pipeActions, ...$actions];
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
	
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipeAction(string $action) : static {
		if(! isset($this->_pipeActions[$action = strtolower($action)])):
			$this->_pipeActions[] = $action;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function deletePipeAction(string $action) : static {
		if(isset($this->_pipeActions[$action = strtolower($action)])):
			unset($this->_pipeActions[$action]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string        $action
	 * @param    mixed|NULL    $value
	 * @param    array         $content
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setPipeAction(string $action, mixed $value = NULL, array $content = []) : array {
		//
		if($this->_checkPipeActionName($action) === TRUE):
			$content[$action] = $value;
		endif;
		
		//
		return $content;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkPipeActionName(string $action) : bool {
		//
		if(in_array($action, $this->_pipeActions ?? [], TRUE) === TRUE):
			return TRUE;
		else:
			$msg = sprintf("Given action '%s' is not supported. Action must be one of following: %s", $action,
			               implode(', ', $this->_pipeActions ?? []));
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * Get pipes from pipeline
	 *
	 * @return Generator
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getPipeline() : Generator;
}
