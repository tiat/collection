<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Pipeline;

//
use Fiber;
use FiberError;
use Generator;
use ReflectionClass;
use ReflectionException;
use Throwable;
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Collection\Exception\RuntimeException;
use Tiat\Standard\Pipeline\PipelineInterface;
use Tiat\Standard\Pipeline\PipelineVars;
use WeakMap;

use function get_debug_type;
use function is_callable;
use function is_object;
use function is_string;
use function method_exists;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Pipeline extends AbstractPipeline {
	
	/**
	 * @var WeakMap
	 * @since   3.0.0 First time introduced.
	 */
	private WeakMap $_pipeline;
	
	/**
	 * Has pipeline been executed or not (default: false = not)
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_pipelineRunStatus = FALSE;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], ...$args) {
		//
		parent::__construct($params, ...$args);
		
		//
		$this->_pipeline = new WeakMap();
	}
	
	/**
	 * @param    null|callable|object    $pipe
	 * @param    null|string             $method
	 * @param    array                   $actions
	 * @param                            ...$args
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerPipe(callable|object|null $pipe, ?string $method = NULL, array $actions = [], ...$args) : PipelineInterface {
		//
		if(is_callable($pipe)):
			//
			if(empty($actions)):
				//
				$actions = $this->_setPipeAction(PipelineVars::PIPE_ACTION_NAME->value, $this->resolvePipeName($pipe),
				                                 $this->_setPipeAction(PipelineVars::PIPE_ACTION_ARGS->value, $args,
				                                                       $this->_setPipeAction(PipelineVars::PIPE_ACTION_METHOD->value,
				                                                                             $method)));
			endif;
			
			//
			$this->setPipeActions($pipe, $actions);
		else:
			$msg = sprintf("Pipe %s is not callable.", get_debug_type($pipe ?? 'no_object'));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    callable|object|string    $pipe
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function resolvePipeName(callable|object|string $pipe) : ?string {
		try {
			return ( new ReflectionClass($pipe) )->getShortName();
		} catch(ReflectionException $e) {
			throw new InvalidArgumentException($e->getMessage());
		}
	}
	
	/**
	 * @param    object|string    $pipe
	 * @param    array            $actions
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeActions(object|string $pipe, array $actions = []) : static {
		//
		if(is_string($pipe)):
			$pipe = $this->findPipe($pipe);
		endif;
		
		//
		$this->_pipeline->offsetSet($pipe, $actions);
		
		//
		return $this;
	}
	
	/**
	 * @param    object|string    $pipe
	 *
	 * @return null|object
	 * @since   3.0.0 First time introduced.
	 */
	public function findPipe(object|string $pipe) : ?object {
		if(is_string($pipe)):
			return $this->findPipeByName($pipe);
		elseif($this->_pipeline->offsetExists($pipe)):
			return $pipe;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|object
	 * @since   3.0.0 First time introduced.
	 */
	public function findPipeByName(string $name) : ?object {
		//
		if($this->_pipeline->count() >= 1):
			//
			$name = strtolower($name);
			$it   = $this->_pipeline->getIterator();
			
			//
			foreach($it as $key => $val):
				if(strtolower($val[PipelineVars::PIPE_ACTION_NAME->value] ?? '') === $name):
					return $key;
				endif;
			endforeach;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    callable    $pipe
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function unregisterPipe(callable $pipe) : PipelineInterface {
		//
		if($this->_pipeline->offsetExists($pipe)):
			$this->_pipeline->offsetUnset($pipe);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return Pipeline
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		if($this->isInitCompleted() === FALSE):
			if($this->getAutoInit() === TRUE):
				$this->init();
			else:
				throw new RuntimeException("Pipeline initialization is missing.");
			endif;
		endif;
		
		// Run the pipeline
		if($this->getPipelineRunStatus() === FALSE && $this->isInitCompleted() === TRUE && $this->checkPipeline() >= 1):
			// There are registered pipe(s) in the pipeline
			foreach($this->_getPipeline() as $pipe => $val):
				// Get the pipe fiber & params
				$fiber = $val[PipelineVars::PIPE_ACTION_FIBER->value];
				$args  = $val[PipelineVars::PIPE_ACTION_ARGS->value] ?? [];
				$name  = $val[PipelineVars::PIPE_ACTION_NAME->value];
				
				// Check the pipe status (don't run it multiple times if not allowed)
				if(( $val[PipelineVars::PIPE_ACTION_STATUS->value] ?? FALSE ) === TRUE):
					throw new RuntimeException(sprintf("Pipe %s has already started.", $name));
				endif;
				
				// Check that there are fiber defined
				if($fiber instanceof Fiber):
					// Do things BEFORE the fiber will be started
					if(! empty($before = $val[PipelineVars::PIPE_ACTION_BEFORE->value] ?? NULL)):
						$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_RESULT_BEFORE->value,
						                     $this->_before($pipe, $before, NULL));
					endif;
					
					// Start the Fiber
					try {
						if($fiber->isStarted() === FALSE):
							if(( $method = $val[PipelineVars::PIPE_ACTION_METHOD->value] ?? NULL ) !== NULL &&
							   method_exists($pipe, $method)):
								$cb = [$pipe, $method];
							elseif(is_callable($pipe)):
								$cb = $pipe;
							else:
								$msg = sprintf("Pipe must be an object with method or direct callable. Got %s.",
								               get_debug_type($pipe));
								throw new InvalidArgumentException($msg);
							endif;
							
							// Get the value from suspend
							// Usually it's the pipe object
							// Is it necessary to save object again? Think not so...fix this later
							$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_SUSPEND->value,
								( $sv = $fiber->start($cb, $args) ));
						else:
							$msg = sprintf("Fiber %s is already started and don't know what todo.", $name);
							throw new RuntimeException($msg);
						endif;
					} catch(Throwable|FiberError $e) {
						throw new RuntimeException($e->getMessage());
					}
					
					// Resume fiber if it's suspended
					if($fiber->isSuspended() === TRUE):
						try {
							// Set the value from suspend() to resume()
							$fiber->resume($sv);
						} catch(Throwable|FiberError $e) {
							$msg = $e->getMessage() . ' (' . $e->getFile() . ' line: ' . $e->getLine() . ')';
							throw new RuntimeException($msg);
						}
					endif;
					
					// Collect the result from the fiber & save it
					if($fiber->isRunning() === FALSE && $fiber->isTerminated() === TRUE):
						try {
							$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_RESULT->value, $fiber->getReturn());
						} catch(Throwable|FiberError $e) {
							throw new RuntimeException($e->getMessage());
						}
					endif;
					
					// Do things AFTER the fiber has been terminated, or it's not running anymore
					if(! empty($after = $val[PipelineVars::PIPE_ACTION_AFTER->value] ?? NULL)):
						$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_RESULT_AFTER->value,
						                     $this->_after($pipe, $after, NULL));
					endif;
					
					// Mark that fiber has been started and prevent n+1 loop
					$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_STATUS->value, TRUE);
				else:
					$msg = sprintf("There are no fiber instance for pipe %s", $name);
					throw new InvalidArgumentException($msg);
				endif;
			endforeach;
			
			// Merge pipeline process status to true if no errors found
			$this->setPipelineRunStatus(TRUE)->setRun($this->getPipelineRunStatus());
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return Pipeline
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		// Prevent multiple init competitions
		if($this->isInitCompleted() === FALSE && $this->checkPipeline() >= 1):
			// Define the Fiber(s)
			foreach($this->_getPipeline() as $pipe => $val):
				//
				$this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_FIBER->value,
				                     new Fiber(function ($cb, $args) : void {
					                     Fiber::suspend($cb);
					                     $cb($args);
				                     }));
			endforeach;
			
			// Set init completed
			$this->setInitCompleted(TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function checkPipeline() : int {
		return $this->_pipeline->count() ?? 0;
	}
	
	/**
	 * @return Generator
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getPipeline() : Generator {
		if(( $result = $this->_pipeline->getIterator() ) !== NULL):
			foreach($result as $key => $val):
				yield $key => $val;
			endforeach;
		else:
			throw new RuntimeException("There are no content in pipeline.");
		endif;
	}
	
	/**
	 * @param    callable|object|string    $pipe
	 * @param    string                    $name
	 * @param    mixed                     $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeAction(callable|object|string $pipe, string $name, mixed $value) : PipelineInterface {
		//
		if(is_string($pipe)):
			$pipe = $this->findPipe($pipe);
		endif;
		
		//
		if(is_object($pipe) || is_string($pipe)):
			$this->setPipeActions($pipe, $this->_setPipeAction($name, $value, $this->getPipeActions($pipe) ?? []));
		else:
			throw new InvalidArgumentException("Variable (pipe) can not be null.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    object|string    $pipe
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeActions(object|string $pipe) : ?array {
		//
		if(is_string($pipe)):
			$pipe = $this->findPipe($pipe);
		endif;
		
		//
		if($this->_pipeline->offsetExists($pipe)):
			return $this->_pipeline->offsetGet($pipe);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipelineRunStatus() : bool {
		if(! empty($this->_pipelineRunStatus)):
			return $this->_pipelineRunStatus;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipelineRunStatus(bool $status) : static {
		//
		$this->_pipelineRunStatus = $status;
		
		//
		return $this;
	}
	
	/**
	 * @param    object        $pipe
	 * @param    callable      $before
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _before(object $pipe, callable $before, mixed $default = NULL) : mixed {
		if(is_callable($before)):
			return $before($pipe) ?? $default;
		else:
			$msg = sprintf('Action for before() must be callable, %s given.', get_debug_type($before));
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @param    object        $pipe
	 * @param    callable      $after
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _after(object $pipe, callable $after, mixed $default = NULL) : mixed {
		// Do things AFTER
		if(is_callable($after)):
			return $after($pipe) ?? $default;
		else:
			$msg = sprintf('Action for after() must be callable, %s given.', get_debug_type($after));
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @param    callable|object|string    $pipe
	 * @param    callable                  $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function after(callable|object|string $pipe, callable $value) : PipelineInterface {
		return $this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_AFTER->value, $value);
	}
	
	/**
	 * @param    callable|object|string    $pipe
	 * @param    callable                  $value
	 *
	 * @return PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function before(callable|object|string $pipe, callable $value) : PipelineInterface {
		return $this->setPipeAction($pipe, PipelineVars::PIPE_ACTION_BEFORE->value, $value);
	}
	
	/**
	 * @param    object|string    $pipe
	 * @param    mixed|NULL       $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult(object|string $pipe, mixed $default = NULL) : mixed {
		return $this->getPipeAction($pipe, PipelineVars::PIPE_ACTION_RESULT->value) ?? $default;
	}
	
	/**
	 * @param    object|string    $pipe
	 * @param    string           $action
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeAction(object|string $pipe, string $action) : mixed {
		//
		if($this->_checkPipeActionName($action = strtolower($action)) === TRUE):
			//
			if(is_string($pipe)):
				$pipe = $this->findPipe($pipe);
			endif;
			
			//
			if($this->_pipeline->offsetExists($pipe) && ! empty($values = $this->_pipeline->offsetGet($pipe))):
				foreach($values as $key => $val):
					if(strtolower($key) === $action):
						return $val;
					endif;
				endforeach;
			endif;
		endif;
		
		//
		return NULL;
	}
}
