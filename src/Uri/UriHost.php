<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Uri;

//
use Laminas\Uri\UriFactory;
use Tiat\Collection\Exception\InvalidArgumentException;

use function array_key_exists;
use function array_reverse;
use function explode;
use function gethostname;
use function implode;
use function in_array;
use function is_string;
use function mb_list_encodings;
use function mb_strtolower;
use function parse_str;
use function preg_match;
use function sprintf;
use function str_contains;
use function strlen;
use function strpos;
use function strtolower;
use function substr;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class UriHost implements UriHostInterface {
	
	/**
	 * List of default host list from $_SERVER superglobals.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_HOST_LIST = ['HTTP_HOST'];
	
	/**
	 * @var string
	 */
	private string $_requestUri;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_encoding = self::DEFAULT_ENCODING;
	
	/**
	 * @param    array    $_request
	 */
	public function __construct(private array $_request = []) {
		if(empty($this->_request)):
			$this->_request = $_SERVER;
		endif;
	}
	
	/**
	 * @return $this
	 */
	public function resetEncoding() : static {
		//
		$this->_encoding = self::DEFAULT_ENCODING;
		
		//
		return $this;
	}
	
	/**
	 * Return domain name without TLD (https://example.com = example). If localhost detected then return localhost.
	 *
	 * @param    NULL|string    $server
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDomainName(?string $server = NULL) : ?string {
		if(! empty($host = UriFactory::factory($server ?? $this->getUrl())->getHost()) &&
		   ! ( new Ip() )->isValid($host, TRUE)):
			// Remove TLD from host name
			if(mb_strtolower($name = $this->getDomainTld($server), $this->getEncoding()) === 'localhost'):
				return $name;
			else:
				$name = array_reverse(explode('.', substr($host, 0,
				                                          strlen($host) - strlen('.' . $this->getDomainTld($server)))));
			endif;
			
			//
			return $name[0];
		endif;
		
		return NULL;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHost() : string {
		//
		$host = $this->getServer('HTTP_HOST');
		
		//
		if(! empty($host)):
			return $host;
		endif;
		
		//
		$scheme = $this->getScheme();
		$name   = $this->getServer('SERVER_NAME');
		$port   = $this->getServer('SERVER_PORT');
		
		//
		if(empty($name)):
			return $name;
		elseif(( $scheme === self::SCHEME_HTTP && $port === 80 ) ||
		       ( $scheme === self::SCHEME_HTTPS && $port === 443 )):
			return $name;
		else:
			return $name . ':' . $port;
		endif;
	}
	
	/**
	 * @param    array|string    $index
	 * @param    null|mixed      $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getServer(array|string $index, mixed $default = NULL) : mixed {
		if(is_string($index)):
			return $this->_request[$index] ?? $default;
		else:
			//
			foreach($index as $val):
				if(isset($this->_request[$val])):
					return $this->_request[$val];
				endif;
			endforeach;
			
			//
			return $default;
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getScheme() : string {
		//
		$value = ( $this->getServer(['HTTPS', 'HTTP_X_FORWARDED_PROTO', 'REQUEST_SCHEME'], 'http') );
		
		//
		if($value === 'on' || $value === self::SCHEME_HTTPS):
			return self::SCHEME_HTTPS;
		else:
			return self::SCHEME_HTTP;
		endif;
	}
	
	/**
	 * Get full request url
	 *
	 * @param    null|string    $url
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl(?string $url = NULL) : ?string {
		//
		$uri = $this->getUri($url);
		
		//
		if(str_contains($uri, self::SCHEME_HTTP) || str_contains($uri, self::SCHEME_HTTPS)):
			return $uri . '?' . $this->getQuery($url) ?? NULL;
		endif;
		
		//
		return ( $this->getScheme() . '://' . $this->getHost() . DIRECTORY_SEPARATOR . $uri . '?' .
		         $this->getQuery($url) ) ?? NULL;
	}
	
	/**
	 * @param    null|string    $uri
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUri(?string $uri = NULL) : ?string {
		//
		$request = $uri ?? $this->getRequestUri();
		
		//
		if($request[0] === DIRECTORY_SEPARATOR):
			$request = substr($request, 1);
		endif;
		
		//
		if(str_contains($request, '?')):
			$request = substr($request, 0, strpos($request, '?'));
		endif;
		
		//
		return $request;
	}
	
	/**
	 * @param    null|string    $uri
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestUri(?string $uri = NULL) : ?string {
		if(empty($this->_requestUri)):
			$this->_setRequestUri($uri);
		endif;
		
		//
		return $this->_requestUri ?? NULL;
	}
	
	/**
	 * @param    null|string    $request
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _setRequestUri(?string $request = NULL) : void {
		//
		if(empty($request)):
			if(isset($this->_request['REQUEST_URI'])):
				$uri = $this->_request['REQUEST_URI'];
			elseif(isset($this->_request['ORIG_PATH_INFO'])):
				// CGI version
				$uri = $this->_request['ORIG_PATH_INFO'];
			endif;
		elseif(( $pos = strpos($request, '?') ) !== FALSE):
			// Get key => value pairs and set $_GET
			$query = substr($request, $pos + 1);
			parse_str($query, $vars);
			
			// @TODO There is no sense with this because $vars is array and it should be string
			$this->getQuery($vars);
		endif;
		
		//
		$this->_requestUri = $uri ?? $request;
	}
	
	/**
	 * @param    null|string    $uri
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(?string $uri = NULL) : string {
		if(empty($uri)):
			$uri = $this->getRequestUri();
			$uri = substr($uri, strpos($uri, '?') + 1);
		endif;
		
		//
		return substr($uri, (int)strpos($uri, '?'));
	}
	
	/**
	 * @param    null|string    $server
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDomainTld(?string $server = NULL) : ?string {
		// Define host
		if(! empty($host = UriFactory::factory($server ?? $this->getUrl())->getHost()) &&
		   ! empty($tmp = array_reverse(explode('.', $host))) && ! ( new Ip() )->isValid($host, TRUE)):
			//
			$ext = $this->_getDomainTldExtended();
			
			//
			if(array_key_exists(strtolower($tmp[0]), $ext)):
				foreach($ext as $val):
					if(in_array(strtolower($tmp[1] ?? NULL), $val, TRUE)):
						return $tmp[1] . '.' . $tmp[0];
					endif;
				endforeach;
			endif;
			
			//
			return $tmp[0];
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Secondary top-level type's for domain TLDs
	 *
	 * @return string[][]
	 * @since   3.0.0 First time introduced.
	 */
	private function _getDomainTldExtended() : array {
		return ['com' => ['us'],
		        'uk' => ['ac', 'co', 'gov', 'ltd', 'me', 'mil', 'mod', 'net', 'nic', 'nhs', 'org', 'plc', 'police',
		                 'sch'],
		        'us' => ['ak', 'al', 'ar', 'az', 'ca', 'co', 'ct', 'dc', 'de', 'dni', 'fed', 'fl', 'ga', 'hi', 'ia',
		                 'id', 'il', 'in', 'isa', 'kids', 'ks', 'ky', 'la', 'ma', 'md', 'me', 'mi', 'mn', 'mo', 'ms',
		                 'mt', 'nc', 'nd', 'ne', 'nh', 'nj', 'nm', 'nsn', 'nv', 'ny', 'oh', 'ok', 'or', 'pa', 'ri',
		                 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wi', 'wv', 'wy'],
		        'au' => ['com', 'net', 'org', 'edu', 'gov', 'asn', 'id'],
		        'ca' => ['ab', 'bc', 'mb', 'nb', 'nf', 'ns', 'nt', 'nu', 'on', 'pe', 'qc', 'sk', 'yk'],
		        'in' => ['co', 'net', 'org', 'edu', 'gov', 'nic', 'res', 'ac'],
		        'fr' => ['assoc', 'nom', 'prd', 'presse', 'tm', 'com', 'gouv'],
		        'jp' => ['co', 'ac', 'ad', 'ed', 'go', 'gr', 'lg', 'ne', 'or'],
		        'cn' => ['ac', 'com', 'edu', 'gov', 'mil', 'net', 'org'],
		        'br' => ['com', 'net', 'org', 'gov', 'edu', 'mil', 'agr', 'art', 'bio', 'eco', 'eng', 'esp', 'eti',
		                 'ind', 'jus', 'lel', 'med', 'mus', 'rec', 'srv', 'tur'],
		        'za' => ['ac', 'co', 'gov', 'law', 'mil', 'net', 'nom', 'org', 'school'],
		        'ru' => ['ac', 'com', 'edu', 'gov', 'mil', 'net', 'org', 'pp']];
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string {
		return $this->_encoding;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : static {
		//
		$f = static function ($haystack, $needle) {
			foreach($haystack as $val):
				if(strtolower($val) === strtolower($needle)):
					return TRUE;
				endif;
			endforeach;
			
			return FALSE;
		};
		
		//
		if($f($values = mb_list_encodings(), $encoding) === TRUE):
			$this->_encoding = $encoding;
		else:
			$msg = sprintf("The encoding %s is not supported by this system. Accepted values are: %s.", $encoding,
			               implode(', ', $values));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Resolves and returns the host based on server variables.
	 *
	 * @param    array    $vars    An array of server variable keys to check for the host. Defaults to a predefined list of keys.
	 *
	 * @return string The resolved host name or the system's default host name if none of the specified keys are found.
	 * @since   3.0.0 First time introduced.
	 */
	public function resolveHost(array $vars = self::DEFAULT_HOST_LIST) : string {
		if(! empty($vars)):
			foreach($vars as $key):
				if(isset($_SERVER[$key])):
					return $_SERVER[$key];
				endif;
			endforeach;
		endif;
		
		//
		return gethostname();
	}
	
	/**
	 * @param    string    $domain
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateDomain(string $domain) : bool {
		return (bool)preg_match("/^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$/i", $domain);
	}
}
