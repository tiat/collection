<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Uri;

/**
 * Interface UriHostInterface
 * An interface defining the structure for URI host implementations.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface UriHostInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SCHEME_HTTP = 'http';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SCHEME_HTTPS = 'https';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_ENCODING = 'UTF-8';
	
	/**
	 * List of default host list from $_SERVER superglobals.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_HOST_LIST = ['HTTP_HOST'];
	
	/**
	 * @return UriHostInterface
	 */
	public function resetEncoding() : UriHostInterface;
	
	/**
	 * Return domain name without TLD (https://example.com = example). If localhost detected then return localhost.
	 *
	 * @param    string    $server
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDomainName(string $server) : ?string;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHost() : string;
	
	/**
	 * @param    array|string    $index
	 * @param    mixed           $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getServer(array|string $index, mixed $default) : mixed;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getScheme() : string;
	
	/**
	 * Get full request url
	 *
	 * @param    string    $url
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl(string $url) : ?string;
	
	/**
	 * @param    string    $uri
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUri(string $uri) : ?string;
	
	/**
	 * @param    string    $uri
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestUri(string $uri) : ?string;
	
	/**
	 * @param    string    $uri
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery(string $uri) : string;
	
	/**
	 * @param    string    $server
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDomainTld(string $server) : ?string;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string;
	
	/**
	 * @param    string    $encoding
	 *
	 * @return UriHostInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : UriHostInterface;
	
	/**
	 * Resolves and returns the host based on server variables.
	 *
	 * @param    array    $vars    An array of server variable keys to check for the host. Defaults to a predefined list of keys.
	 *
	 * @return string The resolved host name or the system's default host name if none of the specified keys are found.
	 * @since   3.0.0 First time introduced.
	 */
	public function resolveHost(array $vars = self::DEFAULT_HOST_LIST) : string;
	
	/**
	 * Validates the provided domain name.
	 *
	 * @param    string    $domain    The domain name to be validated.
	 *
	 * @return bool True if the domain is valid, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function validateDomain(string $domain) : bool;
}