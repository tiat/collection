<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Uri;

//
use Tiat\Collection\Exception\InvalidArgumentException;

use function explode;
use function filter_var;
use function gmp_init;
use function gmp_strval;
use function implode;
use function inet_ntop;
use function inet_pton;
use function ip2long;
use function is_string;
use function long2ip;
use function sprintf;
use function str_contains;
use function str_pad;
use function str_replace;
use function str_split;
use function strlen;
use function trim;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Ip implements IpInterface {
	
	/**
	 * @param    null|string|array    $ip
	 * @param    bool                 $validate
	 * @param    bool                 $private
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function clientIp(string|array|null $ip = NULL, bool $validate = TRUE, bool $private = TRUE) : string {
		//
		if(empty($ip)):
			$ip = $this->_getClientIp();
		endif;
		
		// If IP has ',' then convert it to array
		if(is_string($ip)):
			if(str_contains($ip, ',')):
				$list = explode(',', $ip);
			else:
				$list = [$ip];
			endif;
		else:
			$list = $ip;
		endif;
		
		// Try validate & raise error if not valid
		if(( $validate === TRUE )):
			foreach($list as $val):
				if($this->isValid(trim($val), $private) === FALSE):
					//
					$msg = sprintf("Given IP ('%s') is not a valid.", $val);
					throw new InvalidArgumentException($msg);
				endif;
			endforeach;
		endif;
		
		//
		return $ip;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _getClientIp() : mixed {
		return $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? $_SERVER['HTTP_CLIENT_IP'];
	}
	
	/**
	 * Is IP valid IPv4 or IPv6
	 *
	 * @param    NULL|string    $ip
	 * @param    bool           $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid(?string $ip = NULL, bool $private = TRUE) : bool {
		if(! $this->isValid4($ip, $private)):
			return $this->isValid6($ip, $private);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * Validate IPv4
	 *
	 * @param    string    $ip
	 * @param    bool      $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid4(string $ip, bool $private = TRUE) : bool {
		return $this->_validIp($ip, FILTER_FLAG_IPV4, $private);
	}
	
	/**
	 * @param    string    $ip
	 * @param    int       $options
	 * @param    bool      $private
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	private function _validIp(string $ip, int $options, bool $private = TRUE) : bool {
		if($private):
			$o = $options;
		else:
			$o = $options | FILTER_FLAG_NO_PRIV_RANGE;
		endif;
		
		//
		return (bool)filter_var($ip, FILTER_VALIDATE_IP, $o);
	}
	
	/**
	 * Validate IPv6
	 *
	 * @param    string    $ip
	 * @param    bool      $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid6(string $ip, bool $private = TRUE) : bool {
		return $this->_validIp($ip, FILTER_FLAG_IPV6, $private);
	}
	
	/**
	 * Convert integer to IPv4
	 *
	 * @param    int    $ip
	 *
	 * @return false|string
	 * @since   3.0.0 First time introduced.
	 */
	public function address4(int $ip) : false|string {
		return long2ip($ip);
	}
	
	/**
	 * Convert IPv4 to integer
	 *
	 * @param    string    $ip
	 *
	 * @return false|int
	 * @since   3.0.0 First time introduced.
	 */
	public function convert4(string $ip) : false|int {
		return ip2long($ip);
	}
	
	/**
	 * Convert INT64 blocks to valid IPv6 address
	 *
	 * @param    null|string    $first
	 * @param    null|string    $second
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function address6(?string $first = NULL, ?string $second = NULL) : ?string {
		if($first !== NULL && $second !== NULL):
			//
			$p1      = $this->_address6($first);
			$p2      = $this->_address6($second);
			$address = $p1 . $p2;
			$counter = 0;
			$result  = "";
			
			//
			while(1):
				if($counter > 0):
					$result .= ':';
				endif;
				
				//
				$result .= substr($address, $counter * 4, 4);
				$counter++;
				
				//
				if($counter >= 8):
					break;
				endif;
			endwhile;
			
			// Return valid IPv6 address
			return inet_ntop(inet_pton($result));
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    NULL|string    $val
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _address6(?string $val = NULL) : string {
		return str_pad(gmp_strval(gmp_init($val), 16), 16, '0', STR_PAD_LEFT);
	}
	
	/**
	 * Convert IPv6 to integer blocks [INT64, INT64]
	 *
	 * @param    string    $ip
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function convert6(string $ip) : array {
		// Remove leading : and expand shortened notation to full notation
		$fixed = str_replace(':', '', $this->ipv6Expand($ip));
		
		// Split into two parts [INT64, INT64]
		$part[0] = gmp_strval(gmp_init(substr($fixed, 0, 16), 16));
		$part[1] = gmp_strval(gmp_init(substr($fixed, 16), 16));
		
		//
		return $part;
	}
	
	/**
	 * Return the expanded IPv6 in correct format with leading zeros
	 *
	 * @param    string    $ip
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function ipv6Expand(string $ip) : string {
		//
		$ip     = trim($ip);
		$length = strlen($ip);
		
		//
		if($length > 0 && $length <= 39):
			$array = explode(':', $ip);
			foreach($array as &$val):
				$val = str_pad($val, 4, '0', STR_PAD_LEFT);
			endforeach;
			
			//
			return implode(':', $array);
		endif;
		
		// Return original because it's ok
		return $ip;
	}
	
	/**
	 * Converts a given string to its IPv6 representation.
	 *
	 * @param    string    $value    The input string to be converted to IPv6 format.
	 *
	 * @return string The IPv6 representation of the input string.
	 * @since   3.0.0 First time introduced.
	 */
	public function convertStringToIpv6(string $value) : string {
		//
		$gmp   = gmp_strval(gmp_init($value), 16);
		$split = str_split(str_pad($gmp, 32, "0", STR_PAD_LEFT), 4);
		
		//
		return $this->ipv6Expand(implode(":", $split));
	}
}
