<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Uri;

/**
 * IpInterface is an interface that defines the methods for working with IP addresses.
 * Classes that implement this interface must provide implementation for the methods defined here.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface IpInterface {
	
	/**
	 * @param    NULL|string|array    $ip
	 * @param    bool                 $validate
	 * @param    bool                 $private
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function clientIp(string|array|null $ip = NULL, bool $validate = TRUE, bool $private = TRUE) : string;
	
	/**
	 * Is IP valid IPv4 or IPv6
	 *
	 * @param    NULL|string    $ip
	 * @param    bool           $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid(?string $ip = NULL, bool $private = TRUE) : bool;
	
	/**
	 * Validate IPv4
	 *
	 * @param    string    $ip
	 * @param    bool      $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid4(string $ip, bool $private = TRUE) : bool;
	
	/**
	 * Validate IPv6
	 *
	 * @param    string    $ip
	 * @param    bool      $private    Accept private IP addresses
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isValid6(string $ip, bool $private = TRUE) : bool;
	
	/**
	 * Convert integer to IPv4
	 *
	 * @param    int    $ip
	 *
	 * @return false|string
	 * @since   3.0.0 First time introduced.
	 */
	public function address4(int $ip) : false|string;
	
	/**
	 * Convert IPv4 to integer
	 *
	 * @param    string    $ip
	 *
	 * @return false|int
	 * @since   3.0.0 First time introduced.
	 */
	public function convert4(string $ip) : false|int;
	
	/**
	 * Convert INT64 blocks to valid IPv6 address
	 *
	 * @param    string    $first
	 * @param    string    $second
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function address6(string $first, string $second) : ?string;
	
	/**
	 * Convert IPv6 to integer
	 *
	 * @param    string    $ip
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function convert6(string $ip) : array;
	
	/**
	 * Return the expanded IPv6 in correct format with leading zeros
	 *
	 * @param    string    $ip
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function ipv6Expand(string $ip) : string;
	
	/**
	 * Converts a given string to its IPv6 representation.
	 *
	 * @param    string    $value    The input string to be converted to IPv6 format.
	 *
	 * @return string The IPv6 representation of the input string.
	 * @since   3.0.0 First time introduced.
	 */
	public function convertStringToIpv6(string $value) : string;
}
