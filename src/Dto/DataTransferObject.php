<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Dto;

//
use function is_string;
use function unserialize;

/**
 * This is generic DTO class which will support Command/Event/Query methods with MessageBus.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DataTransferObject extends AbstractDataTransferObject {
	
	/**
	 * @param    mixed    $data
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $data) {
		//
		parent::__construct($data);
		
		//
		if(is_string($data)):
			if(@unserialize($data, $this->_unserializeOptions) !== FALSE):
				$this->_serialized = $this->serialize();
			endif;
		else:
			// Data is already serialized so don't make it again.
			$this->_serialized = $this->serialize();
		endif;
	}
}
