<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Dto;

//
use Tiat\Collection\Exception\InvalidArgumentException;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

use function sprintf;

/**
 * Params for Data Transfer Object module (DTO).
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DataTransferObjectParams: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DTO_PARAM = '';
	
	/**
	 * @param    DataTransferObjectParams    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDescription(DataTransferObjectParams $value) : string {
		return match ( $value ) {
			default => throw new InvalidArgumentException(sprintf("No description for value %s", $value->value))
		};
	}
}
