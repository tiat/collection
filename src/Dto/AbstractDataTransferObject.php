<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Dto;

//
use Tiat\Collection\Exception\InvalidArgumentException;

use function hash_algos;
use function in_array;
use function is_array;
use function serialize;
use function sprintf;
use function strtolower;

/**
 * @todo    Serialize data again before it is returned and stored to the database. DTO can be changed after it has been serialized.
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDataTransferObject implements DataTransferObjectInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_HASH = 'sha256';
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected mixed $_data;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_serialized;
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected mixed $_unserialized;
	
	/**
	 * @var array|true[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_unserializeOptions = ['allowed_classes' => TRUE];
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_hashAlgo = self::DEFAULT_HASH;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_hash;
	
	/**
	 * @param    mixed    $data
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $data) {
		//
		$this->_data = $data;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getData() : mixed {
		return $this->_data ?? NULL;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHash() : string {
		if(empty($this->_hash)):
			$this->generateHash();
		endif;
		
		//
		return $this->_hash;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function generateHash() : void {
		if(! empty($content = $this->toString())):
			$this->_hash = hash($this->getHashAlgo(), $content);
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	final public function toString() : string {
		return $this->serialize();
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	final public function serialize() : string {
		return serialize(get_object_vars($this));
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHashAlgo() : string {
		return $this->_hashAlgo;
	}
	
	/**
	 * @param    string    $hash
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setHashAlgo(string $hash = self::DEFAULT_HASH) : static {
		if($this->checkHash($hash = strtolower($hash)) === TRUE):
			$this->_hashAlgo = $hash;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $hash
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkHash(string $hash = self::DEFAULT_HASH) : bool {
		if(in_array($hash, hash_algos(), TRUE) === FALSE):
			$msg = sprintf("Given hash algo (%s) is not supported in this system. Supported hash algo are: %s", $hash,
			               implode(', ', hash_algos()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	final public function unserialize() : mixed {
		//
		$this->__unserialize($this->_data);
		
		//
		return $this->_unserialized ?? NULL;
	}
	
	/**
	 * @param    string|array    $data
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	final public function __unserialize(string|array $data) : void {
		if(is_array($data)):
			$this->_unserialized = unserialize($data[0], $this->_unserializeOptions);
		else:
			$this->_unserialized = unserialize($data, $this->_unserializeOptions);
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	final public function __toString() : string {
		return $this->toString();
	}
}
