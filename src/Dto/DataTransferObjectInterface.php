<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Collection\Dto;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DataTransferObjectInterface {
	
	/**
	 * Check that hash algo is supported in this system
	 *
	 * @param    string    $hash
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkHash(string $hash) : bool;
	
	/**
	 * Generate hash from the DTO content
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function generateHash() : void;
	
	/**
	 * Get generated hash from DTO content.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHash() : string;
	
	/**
	 * Get used hash algo
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHashAlgo() : string;
	
	/**
	 * Set used hash algo
	 *
	 * @param    string    $hash
	 *
	 * @return DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setHashAlgo(string $hash) : DataTransferObjectInterface;
	
	/**
	 * Return DTO as string
	 * This will serialize the object
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string;
}
