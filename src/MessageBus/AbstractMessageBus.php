<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\MessageBus;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMessageBus implements MessageBusInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int PRIORITY_LOW = PHP_INT_MIN;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int PRIORITY_HIGH = PHP_INT_MAX;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_dtoMessage;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_priority;
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getPriority() : ?int {
		return $this->_priority ?? NULL;
	}
	
	/**
	 * @param    int    $priority
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPriority(int $priority) : MessageBusInterface {
		//
		if($priority >= self::PRIORITY_LOW && $priority <= self::PRIORITY_HIGH):
			$this->_priority = $priority;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init() : MessageBusInterface {
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : bool {
		return TRUE;
	}
}
