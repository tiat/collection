<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\MessageBus;

//
use Tiat\Collection\Dto\DataTransferObjectInterface;

/**
 * Command: its intent is about expressing an order to the system and modifies its current state. User expects no response.
 * Event: its intent is to express something that has already happened in the system, and record it. User expects no response.
 * Query: its intent is about expressing a question to the system. User expects a response.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class MessageBus extends AbstractMessageBus {
	
	/**
	 * @param    string    $name
	 *
	 * @return null|DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessage(string $name) : ?DataTransferObjectInterface {
		return $this->_dtoMessage[$name] ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessages() : ?array {
		return $this->_dtoMessage ?? NULL;
	}
	
	/**
	 * @param    array    $messages
	 *
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessages(iterable $messages) : MessageBusInterface {
		//
		if(! empty($messages)):
			foreach($messages as $key => $val):
				$this->setMessage($key, $val);
			endforeach;
		endif;
		
		return $this;
	}
	
	/**
	 * @param    string                         $name
	 * @param    DataTransferObjectInterface    $dto
	 *
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(string $name, DataTransferObjectInterface $dto) : MessageBusInterface {
		//
		$this->_dtoMessage[$name] = $dto;
		
		//
		return $this;
	}
}
