<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\MessageBus;

//
use Tiat\Collection\Dto\DataTransferObjectInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface MessageBusInterface {
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getPriority() : ?int;
	
	/**
	 * @param    int    $priority
	 *
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPriority(int $priority) : MessageBusInterface;
	
	/**
	 * Get DTO by name.
	 *
	 * @param    string    $name
	 *
	 * @return null|DataTransferObjectInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessage(string $name) : ?DataTransferObjectInterface;
	
	/**
	 * Get all DTO's at once.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessages() : ?array;
	
	/**
	 * Set new DTO with name.
	 *
	 * @param    string                         $name
	 * @param    DataTransferObjectInterface    $dto
	 *
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(string $name, DataTransferObjectInterface $dto) : MessageBusInterface;
	
	/**
	 * Set DTO messages en masse where key is the name and value is DTO object.
	 *
	 * @param    iterable    $messages
	 *
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessages(iterable $messages) : MessageBusInterface;
	
	/**
	 * @return MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init() : MessageBusInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : bool;
}
