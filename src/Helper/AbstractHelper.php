<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Collection
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Collection\Helper;

//
use Tiat\Standard\Plugin\PluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractHelper implements HelperInterface {
	
	/**
	 * @var PluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	private PluginInterface $_plugin;
	
	/**
	 * @return null|PluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPlugin() : ?PluginInterface {
		return $this->_plugin ?? NULL;
	}
	
	/**
	 * @param    PluginInterface    $plugin
	 *
	 * @return HelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPlugin(PluginInterface $plugin) : HelperInterface {
		//
		$this->_plugin = $plugin;
		
		//
		return $this;
	}
}
